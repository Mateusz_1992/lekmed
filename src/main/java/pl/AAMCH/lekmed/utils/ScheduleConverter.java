package pl.AAMCH.lekmed.utils;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.AAMCH.lekmed.domain.Schedule;
import pl.AAMCH.lekmed.service.ScheduleService;

public class ScheduleConverter implements Converter<String, Set<Schedule>> 
{

	@Autowired
	ScheduleService scheduleService;
	
	@Override
	public Set<Schedule> convert(String source) {
		
		Set<Schedule> scheduleRoleList = new HashSet<Schedule>(0);
		
		scheduleRoleList.add(scheduleService.getSchedule(Integer.parseInt(source)));
		
		return scheduleRoleList;
	}

}
