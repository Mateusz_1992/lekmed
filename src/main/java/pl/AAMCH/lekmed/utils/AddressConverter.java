package pl.AAMCH.lekmed.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.AAMCH.lekmed.domain.Address;
import pl.AAMCH.lekmed.service.AddressService;

public class AddressConverter implements Converter<String, Address> {

	@Autowired
	AddressService addressService;
	
	@Override
	public Address convert(String source) {
		return addressService.getAddress(Integer.parseInt(source));
	}
}