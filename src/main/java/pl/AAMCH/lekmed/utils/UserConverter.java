package pl.AAMCH.lekmed.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.service.UserService;

public class UserConverter implements Converter<String, User> {

	@Autowired
	UserService userService;
	
	@Override
	public User convert(String source) {
		return userService.getUser(Integer.parseInt(source));
	}
}