package pl.AAMCH.lekmed.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.AAMCH.lekmed.domain.Room;
import pl.AAMCH.lekmed.service.RoomService;

public class RoomConverter implements Converter<String, Room> {

	@Autowired
	RoomService roomService;
	
	@Override
	public Room convert(String source) {
		return roomService.getRoom(Integer.parseInt(source));
	}
}
