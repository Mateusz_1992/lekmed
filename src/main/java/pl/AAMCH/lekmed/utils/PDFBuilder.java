package pl.AAMCH.lekmed.utils;

import java.util.Map;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import pl.AAMCH.lekmed.domain.Receipt;
 
/**
 * This view class generates a PDF document 'on the fly' based on the data
 * contained in the model.
 * @author www.codejava.net
 *
 */
public class PDFBuilder extends AbstractITextPdfView {
 
    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document doc,
            PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // get data model which is passed by the Spring container
        Receipt rachunek = (Receipt) model.get("rachunek");
         
        doc.add(new Paragraph("LEKMED - rachunek"));
         
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[] {3.0f, 2.0f, 2.0f, 2.0f, 1.0f});
        table.setSpacingBefore(10);
         
        // define font for table header row
        
        String url = request.getRequestURL().toString();
        String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath();
        String path = baseURL + "/resources/fonts/Arial.ttf";
        FontFactory.register(path, "font");
        Font font = FontFactory.getFont("font");
        
      //  Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.WHITE);
         
        // define table header cell
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setPadding(5);
         
        // write table header
        cell.setPhrase(new Phrase("Data", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Lekarz", font));
        table.addCell(cell);
 
        cell.setPhrase(new Phrase("Specjalizacja", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Pacjent", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Cena", font));
        table.addCell(cell);
         
        // write table row data
        
            table.addCell(rachunek.getAppointment().getDate());
            table.addCell(rachunek.getAppointment().getUser().getName()+" "+rachunek.getAppointment().getUser().getSurname());
            table.addCell(rachunek.getAppointment().getUser().getRoom().getSpecialization());
            table.addCell(rachunek.getAppointment().getPatient().getName()+" "+rachunek.getAppointment().getPatient().getSurname());
            table.addCell(String.valueOf(rachunek.getCost())+ " PLN");
        
         
        doc.add(table);
         
    }
 
}
