package pl.AAMCH.lekmed.utils;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.AAMCH.lekmed.domain.Schedule;
import pl.AAMCH.lekmed.service.ScheduleService;

public class ScheduleListConverter implements Converter<String[], Set<Schedule>>
{
	@Autowired
	ScheduleService scheduleService;
	
	@Override
	public Set<Schedule> convert(String[] source) 
	{
		
		Set<Schedule> scheduleList = new HashSet<Schedule>(0);
		
		for (int i=0; i < source.length; i++)
		{
			scheduleList.add(scheduleService.getSchedule(Integer.parseInt(source[i])));
		}
		
		return scheduleList;
	}
	
}
