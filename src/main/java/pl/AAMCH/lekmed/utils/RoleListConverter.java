package pl.AAMCH.lekmed.utils;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.service.UserService;

public class RoleListConverter implements Converter<String[], Set<Role>> {

	@Autowired
	UserService userService;
	
	@Override
	public Set<Role> convert(String[] source) 
	{
		
		Set<Role> userRoleList = new HashSet<Role>(0);
		
		for (int i=0; i < source.length; i++)
		{
			//System.out.println("role id: " + source[i]);
			userRoleList.add(userService.getUserRole(Integer.parseInt(source[i])));
		}
		
		return userRoleList;
	}

}