package pl.AAMCH.lekmed.dao;

import java.util.List;

import pl.AAMCH.lekmed.domain.Room;

public interface RoomDAO {

	public void addRoom(Room room);
	public List<Room> listRoom();
	public void removeRoom (int id);
	public Room getRoom(int id);
	public void editRoom(Room room);
	
}
