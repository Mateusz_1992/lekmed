package pl.AAMCH.lekmed.dao;

import java.util.List;

import pl.AAMCH.lekmed.domain.Schedule;

public interface ScheduleDAO 
{
	public void addSchedule(Schedule userSchedule);
	public List<Schedule> listSchedule();
	public void removeSchedule (int id);
	public Schedule getSchedule(int id);
	public void editSchedule(Schedule schedule);
	
}
