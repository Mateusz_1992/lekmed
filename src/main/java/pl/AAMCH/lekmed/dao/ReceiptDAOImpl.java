package pl.AAMCH.lekmed.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.AAMCH.lekmed.domain.Appointment;
import pl.AAMCH.lekmed.domain.Receipt;

@Repository
public class ReceiptDAOImpl implements ReceiptDAO {

	@Autowired
    SessionFactory sessionFactory;
	
	@Override
	public void addReceipt(Receipt receipt) 
	{
		sessionFactory.getCurrentSession().save(receipt);
	}

	@Override
	public List<Receipt> listReceipt() 
	{	
		return sessionFactory.getCurrentSession().createQuery("from Receipt order by id").list();
	}

	@Override
	public void removeReceipt(int id) {
		Receipt receipt = (Receipt) sessionFactory.getCurrentSession().load(Receipt.class, id);
		
        if (null != receipt) 
        {
            sessionFactory.getCurrentSession().delete(receipt);
        }
	}

	@Override
	public Receipt getReceipt(int id) 
	{
		return (Receipt)sessionFactory.getCurrentSession().get(Receipt.class, id);
	}

	@Override
	public void editReceipt(Receipt receipt) 
	{
		sessionFactory.getCurrentSession().update(receipt);
		
	}

}
