package pl.AAMCH.lekmed.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.AAMCH.lekmed.domain.Schedule;

@Repository
public class ScheduleDAOImpl implements ScheduleDAO 
{	
	@Autowired
    SessionFactory sessionFactory;
	
	
	@Transactional
	public void addSchedule(Schedule schedule) {
		sessionFactory.getCurrentSession().save(schedule);
		
	}

	@Transactional
	public List<Schedule> listSchedule() {
		
		return sessionFactory.getCurrentSession().createQuery("from Schedule order by id").list();
	}

	@Transactional
	public void removeSchedule(int id) {
		Schedule schedule = (Schedule) sessionFactory.getCurrentSession().load(
				Schedule.class, id);
        if (null != schedule) {
            sessionFactory.getCurrentSession().delete(schedule);
        }
		
	}

	@Transactional
	public Schedule getSchedule(int id) 
	{
		return  (Schedule)sessionFactory.getCurrentSession().get(Schedule.class, id);
		
	}

	@Transactional
	public void editSchedule(Schedule schedule) {
    	
    	sessionFactory.getCurrentSession().update(schedule);
    }
	
}
