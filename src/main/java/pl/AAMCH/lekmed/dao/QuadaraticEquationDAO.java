package pl.AAMCH.lekmed.dao;

import java.util.List;

import pl.AAMCH.lekmed.domain.QuadaraticEquation;

public interface QuadaraticEquationDAO {
	public void addAppointment(QuadaraticEquation addEquation);
	
	public QuadaraticEquation getQuadaraticEquation(int eqId);
	/*public List<QuadaraticEquation> listEquations();
	public void removeEquation (int id);
	public QuadaraticEquation getEquation(int id);
	public void editEquation(QuadaraticEquation appointment);*/
}
