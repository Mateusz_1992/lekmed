package pl.AAMCH.lekmed.dao;

import java.util.List;

import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.domain.Role;

public interface UserDAO {

	public void addUser(User user);
	public List<User> listUser();
	public void removeUser (int id);
	public User getUser(int id);
	public void editUser(User user);
	public User findByLogin(String login);
	
	public void addRole(Role userRole);
	public List<Role> listUserRole();
	public void removeUserRole (int id);
	public Role getUserRole(int id);
	public Role findRoleByName(String role);
	public void editUserPassword(User user);
	
	
}