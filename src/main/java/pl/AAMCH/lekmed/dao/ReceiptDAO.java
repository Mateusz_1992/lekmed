package pl.AAMCH.lekmed.dao;

import java.util.List;

import pl.AAMCH.lekmed.domain.Receipt;

public interface ReceiptDAO {
	
	public void addReceipt(Receipt receipt);
	public List<Receipt> listReceipt();
	public void removeReceipt (int id);
	public Receipt getReceipt(int id);
	public void editReceipt(Receipt receipt);

}
