package pl.AAMCH.lekmed.dao;

import java.util.List;



import pl.AAMCH.lekmed.domain.Appointment;

public interface AppointmentDAO {
	public void addAppointment(Appointment appointment);
	public List<Appointment> listAppointment();
	public void removeAppointment (int id);
	public Appointment getAppointment(int id);
	public void editAppointment(Appointment appointment);
}
