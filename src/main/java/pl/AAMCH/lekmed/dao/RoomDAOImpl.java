package pl.AAMCH.lekmed.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.AAMCH.lekmed.domain.Address;
import pl.AAMCH.lekmed.domain.Room;

@Repository
public class RoomDAOImpl implements RoomDAO {

	@Autowired
    SessionFactory sessionFactory;
	
	@Override
	public void addRoom(Room room) 
	{
		sessionFactory.getCurrentSession().save(room);	
	}

	@Override
	public List<Room> listRoom() 
	{
		return sessionFactory.getCurrentSession().createQuery("from Room order by id").list();
	}

	@Override
	public void removeRoom(int id) {
		Room room = (Room) sessionFactory.getCurrentSession().load(
                Room.class, id);
        if (null != room) 
        {
            sessionFactory.getCurrentSession().delete(room);
        }
	}

	@Override
	public Room getRoom(int id) 
	{
		return (Room)sessionFactory.getCurrentSession().get(Room.class, id);
	}

	@Override
	public void editRoom(Room room) 
	{
		sessionFactory.getCurrentSession().update(room);	
	}

}
