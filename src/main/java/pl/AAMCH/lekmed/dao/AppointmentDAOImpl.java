package pl.AAMCH.lekmed.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.AAMCH.lekmed.domain.Appointment;

@Repository
public class AppointmentDAOImpl implements AppointmentDAO{

	@Autowired
    SessionFactory sessionFactory;
	
	public void addAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(appointment);
	}

	public List<Appointment> listAppointment() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Appointment order by id").list();
	}

	public void removeAppointment(int id) {
		// TODO Auto-generated method stub
		Appointment appointment = (Appointment) sessionFactory.getCurrentSession().load(Appointment.class, id);
		
        if (null != appointment) 
        {
            sessionFactory.getCurrentSession().delete(appointment);
        }
	}

	public Appointment getAppointment(int id) {
		// TODO Auto-generated method stub
    	return (Appointment)sessionFactory.getCurrentSession().get(Appointment.class, id);
	}

	public void editAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
    	sessionFactory.getCurrentSession().update(appointment);
	}
}
