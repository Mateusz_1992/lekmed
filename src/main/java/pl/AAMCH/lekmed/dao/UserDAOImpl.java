package pl.AAMCH.lekmed.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.domain.User;


@Repository
public class UserDAOImpl implements UserDAO{

	@Autowired
    SessionFactory sessionFactory;
	
    public void addUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }
    
    public List<User> listUser() {
 
        return sessionFactory.getCurrentSession().createQuery("from User order by id").list();
    }
 
    public void removeUser(int id) {
        User user = (User) sessionFactory.getCurrentSession().load(
                User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
        }
 
    }
    public User getUser(int id) {
    	
    	return (User)sessionFactory.getCurrentSession().get(User.class, id);
    }
    
  public void editUser(User user) {
    	
    	sessionFactory.getCurrentSession().update(user);
    }
	
  
  @SuppressWarnings("unchecked")
  public User findByLogin(String login)
  {
	  List<User> users = new ArrayList<User>();
	  
		users = sessionFactory.getCurrentSession()
			.createQuery("from User where login=?")
			.setParameter(0, login)
			.list();

		if (users.isEmpty()) 
		{
            return null;
        }
		else
		{
			return users.get(0);
		}
  }
  
    public void addRole(Role userRole)
    {
    	sessionFactory.getCurrentSession().save(userRole);
    }
    
	public List<Role> listUserRole() {
		 return sessionFactory.getCurrentSession().createQuery("from Role order by id").list();
	}
	
	public void removeUserRole (int id) {
		Role role = (Role) sessionFactory.getCurrentSession().load(
                Role.class, id);
        if (null != role) {
            sessionFactory.getCurrentSession().delete(role);
        }
	}
	
	public Role getUserRole(int id) {
		return (Role)sessionFactory.getCurrentSession().get(Role.class, id);
	}
    
    @SuppressWarnings("unchecked")
    public Role findRoleByName(String role)
    {

  	  List<Role> userRole = new ArrayList<Role>();
  	  
  	  userRole = sessionFactory.getCurrentSession()
  			.createQuery("from Role where roleName=?")
  			.setParameter(0, role)
  			.list();

  		if (userRole.size() > 0) {
  			return userRole.get(0);
  		} else {
  			return null;
  		}
    }

	@Override
	public void editUserPassword(User user) {
		sessionFactory.getCurrentSession().update(user);
		
	}

}
