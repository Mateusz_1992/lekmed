package pl.AAMCH.lekmed.service;

import java.util.List;

import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.domain.User;

public interface UserService {

	public void addUser(User user);
	public void adminAddUser(User user);
	public void editUser(User user);
	public List<User> listUser();
	public void removeUser (int id);
	public User getUser(int id);
	
	public String hashPassword(String password);
	
	public void addUserRole(Role userRole);
	public List<Role> listUserRole();
	public void removeUserRole (int id);
	public Role getUserRole(int id);
	public User findByLogin(String name);
	public Role findRoleByName(String string);
	public void editUserPassword(User user);
}