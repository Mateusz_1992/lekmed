package pl.AAMCH.lekmed.service;

import java.util.List;

import pl.AAMCH.lekmed.domain.Schedule;

public interface ScheduleService 
{
	public void addSchedule(Schedule schedule);
	public List<Schedule> listSchedule();
	public void removeSchedule (int id);
	public Schedule getSchedule(int id);
	public void editSchedule(Schedule schedule);
}
