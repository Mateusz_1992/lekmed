package pl.AAMCH.lekmed.service;

import java.util.List;

import pl.AAMCH.lekmed.domain.Appointment;

public interface AppointmentService {
	public void addAppointment(Appointment appointment);
	public void editAppointment(Appointment appointment);
	public List<Appointment> listAppointment();
	public void removeAppointment (int id);
	public Appointment getAppointment(int id);
}
