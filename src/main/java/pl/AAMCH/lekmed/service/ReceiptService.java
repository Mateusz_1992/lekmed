package pl.AAMCH.lekmed.service;

import java.util.List;

import pl.AAMCH.lekmed.domain.Receipt;

public interface ReceiptService 
{
	public void addReceipt(Receipt receipt);
	public void editReceipt(Receipt receipt);
	public List<Receipt> listReceipt();
	public void removeReceipt (int id);
	public Receipt getReceipt(int id);

}
