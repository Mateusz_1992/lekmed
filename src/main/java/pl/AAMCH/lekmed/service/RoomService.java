package pl.AAMCH.lekmed.service;

import java.util.List;

import pl.AAMCH.lekmed.domain.Room;

public interface RoomService 
{

	public void addRoom(Room room);
	public void editRoom(Room room);
	public List<Room> listRoom();
	public void removeRoom (int id);
	public Room getRoom(int id);
}
