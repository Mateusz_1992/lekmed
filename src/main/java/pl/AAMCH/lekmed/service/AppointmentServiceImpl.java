package pl.AAMCH.lekmed.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.AAMCH.lekmed.dao.AppointmentDAO;
import pl.AAMCH.lekmed.domain.Appointment;

@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService{

	@Autowired
	AppointmentDAO appointmentDAO;
	
	@Transactional
	public void addAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		appointmentDAO.addAppointment(appointment);
	}

	@Transactional
	public void editAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		appointmentDAO.addAppointment(appointment);
	}

	@Transactional
	public List<Appointment> listAppointment() {
		// TODO Auto-generated method stub
		return appointmentDAO.listAppointment();
	}

	@Transactional
	public void removeAppointment(int id) {
		// TODO Auto-generated method stub
		appointmentDAO.removeAppointment(id);
	}

	@Transactional
	public Appointment getAppointment(int id) {
		// TODO Auto-generated method stub
		return appointmentDAO.getAppointment(id);
	}

}
