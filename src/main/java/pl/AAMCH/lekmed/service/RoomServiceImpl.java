package pl.AAMCH.lekmed.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.AAMCH.lekmed.dao.RoomDAO;
import pl.AAMCH.lekmed.domain.Room;

@Service
@Transactional
public class RoomServiceImpl implements RoomService
{
	@Autowired
	RoomDAO roomDAO;
	
	@Override
	public void addRoom(Room room) 
	{
		roomDAO.addRoom(room);
	}

	@Override
	public void editRoom(Room room) 
	{
		roomDAO.editRoom(room);	
	}

	@Override
	public List<Room> listRoom() 
	{
		return roomDAO.listRoom();
	}

	@Override
	public void removeRoom(int id) 
	{
		roomDAO.removeRoom(id);
	}

	@Override
	public Room getRoom(int id) 
	{
		return roomDAO.getRoom(id);
	}

}
