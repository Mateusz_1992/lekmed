package pl.AAMCH.lekmed.service;

import pl.AAMCH.lekmed.domain.QuadaraticEquation;

public interface QuadraticEquationService {
	public void addUser(QuadaraticEquation equation);
	public QuadaraticEquation getQuadaraticEquation(int eqId);
}
