package pl.AAMCH.lekmed.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.AAMCH.lekmed.dao.ReceiptDAO;
import pl.AAMCH.lekmed.domain.Receipt;

@Service
@Transactional
public class ReceiptServiceImpl implements ReceiptService {

	@Autowired
	ReceiptDAO receiptDAO;
	
	@Override
	public void addReceipt(Receipt receipt) 
	{
		receiptDAO.addReceipt(receipt);
	}

	@Override
	public void editReceipt(Receipt receipt)
	{
		receiptDAO.editReceipt(receipt);	
	}

	@Override
	public List<Receipt> listReceipt() 
	{
		return receiptDAO.listReceipt();
	}

	@Override
	public void removeReceipt(int id) 
	{
		receiptDAO.removeReceipt(id);
	}

	@Override
	public Receipt getReceipt(int id) 
	{
		return receiptDAO.getReceipt(id);
	}

}
