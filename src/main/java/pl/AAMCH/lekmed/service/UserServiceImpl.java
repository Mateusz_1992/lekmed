package pl.AAMCH.lekmed.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.AAMCH.lekmed.dao.UserDAO;
import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.domain.Role;

@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	UserDAO userDAO;
	
	
	@Transactional
	public void addUser(User user) {
		user.getUserRole().add(userDAO.findRoleByName("ROLE_CLIENT"));
		user.setPassword(hashPassword(user.getPassword()));
		userDAO.addUser(user);
	}
	
	@Transactional
	public void adminAddUser(User user) {
		user.getUserRole().add(userDAO.findRoleByName("ROLE_CLIENT"));
		user.setTokenActivate(UUID.randomUUID().toString());
		user.setPassword(hashPassword("password"));
		userDAO.addUser(user);
	}
	
	@Transactional
	public void editUser(User user) 
	{
		userDAO.editUser(user);
	}

	@Transactional
	public List<User> listUser() {
		
		return userDAO.listUser();
	}

	@Transactional
	public void removeUser(int id) {
		userDAO.removeUser(id);
	}
	
	@Transactional
	public User getUser(int id) {
		return userDAO.getUser(id);
	}
	
	@Transactional
	public String hashPassword(String password)
	{
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}
	
	@Transactional
	public void addUserRole(Role userRole) {
		userDAO.addRole(userRole);
	}
	
	@Transactional
	public List<Role> listUserRole() {
		return userDAO.listUserRole();
	}
	
	@Transactional
	public void removeUserRole (int id) {
		userDAO.removeUserRole(id);
	}
	
	@Transactional
	public Role getUserRole(int id) {
		return userDAO.getUserRole(id);
	}

	@Override
	public User findByLogin(String name) {
		return userDAO.findByLogin(name);
		 
	}

	@Override
	public Role findRoleByName(String string) {
		return userDAO.findRoleByName(string);
		 
	}

	@Override
	public void editUserPassword(User user) {
		user.setPassword(hashPassword(user.getPassword()));
		userDAO.editUserPassword(user);
		
	}

}
