package pl.AAMCH.lekmed.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.AAMCH.lekmed.dao.ScheduleDAO;
import pl.AAMCH.lekmed.domain.Schedule;

@Service
@Transactional
public class ScheduleServiceImpl implements ScheduleService
{

	@Autowired
	ScheduleDAO scheduleDAO;
	
	@Transactional
	public void addSchedule(Schedule schedule) {
		scheduleDAO.addSchedule(schedule);
		
	}

	@Transactional
	public List<Schedule> listSchedule() {
		
		return scheduleDAO.listSchedule();
	}

	@Transactional
	public void removeSchedule(int id) {
		scheduleDAO.removeSchedule(id);
		
	}

	@Transactional
	public Schedule getSchedule(int id) {
		
		return scheduleDAO.getSchedule(id);
	}

	@Transactional
	public void editSchedule(Schedule schedule){
		
		scheduleDAO.editSchedule(schedule);
	}
}
