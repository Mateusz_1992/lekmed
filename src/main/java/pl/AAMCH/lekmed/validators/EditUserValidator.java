package pl.AAMCH.lekmed.validators;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.AAMCH.lekmed.domain.User;

public class EditUserValidator implements Validator
{
	private static String telephoneRegEx = new String("\\+48-[0-9]{3}-[0-9]{3}-[0-9]{3}");
	RegexValidator phoneValidator = new RegexValidator(telephoneRegEx);
	  
	private static String peselRegEx = new String("[0-9]{11}");
	RegexValidator peselValidator = new RegexValidator(peselRegEx);
	
	EmailValidator emailValidator = EmailValidator.getInstance();
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return User.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object arg0, Errors arg1)
	{
		// TODO Auto-generated method stub
	}
	
	public void validate(User user, Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors,"name", "editUserValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"surname", "editUserValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"phoneNumber", "editUserValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"email", "editUserValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"pesel", "editUserValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"dataUrodzenia", "editUserValidator.wymagane");
		
		if (errors.getErrorCount() == 0) 
		{
			if (StringUtils.hasText(user.getEmail()) && emailValidator.isValid(user.getEmail()) == false)
			{
				errors.rejectValue("email", "editUserValidator.email.invalid");
			}
			if (StringUtils.hasText(user.getPhoneNumber()) && phoneValidator.isValid(user.getPhoneNumber()) == false)
			{
				errors.rejectValue("phoneNumber", "editUserValidator.phoneNumber.invalid");
			}
			if (StringUtils.hasText(user.getPesel()) && peselValidator.isValid(user.getPesel()) == false)
			{
				errors.rejectValue("pesel", "editUserValidator.pesel.invalid");
			}
			
			String dataString, peselString, rokD, miesiacD, dzienD, rokP, miesiacP, dzienP;
			
			
			if(user.getDataUrodzenia() != null)
			{
				dataString = user.getDataUrodzenia();
				 rokD = dataString.substring(2, 3);
				 miesiacD = dataString.substring(5, 6);
				 dzienD = dataString.substring(8, 9);
			}
			else
			{
				 dataString = new String("");
				 rokD = new String("");
				 miesiacD = new String("");
				 dzienD = new String("");
			}
			if(!user.getPesel().isEmpty())
			{
				peselString = user.getPesel();
				rokP = peselString.substring(0, 1);
				miesiacP = peselString.substring(2, 3);
				dzienP = peselString.substring(4, 5);
			}
			else
			{
				rokP = new String("0");
			 	miesiacP = new String("0");
			 	dzienP = new String("0");
			}
			if(!rokD.equals(rokP) || !miesiacD.equals(miesiacP) || !dzienD.equals(dzienP))
			{
				errors.rejectValue("dataUrodzenia", "editUserValidator.date.invalid");
			}
		} 
  }
}
