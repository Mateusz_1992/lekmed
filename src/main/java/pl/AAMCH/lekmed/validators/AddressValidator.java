package pl.AAMCH.lekmed.validators;
import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.AAMCH.lekmed.domain.Address;

public class AddressValidator implements Validator
{
	private static String postCodeRegEx = new String("[0-9]{2}-[0-9]{3}");
	RegexValidator postCodeValidator = new RegexValidator(postCodeRegEx);

	private static String flatNumberRegEx = new String("[0-9]*");
	RegexValidator flatNumberValidator = new RegexValidator(flatNumberRegEx);
	
	private static String homeNumberRegEx = new String("[0-9]+");
	RegexValidator homeNumberValidator = new RegexValidator(homeNumberRegEx);

	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return Address.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object arg0, Errors arg1)
	{
		// TODO Auto-generated method stub
	}
	
	public void validate(Address address, Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors,"street", "addressValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"homeNumber", "addressValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"postCode", "addressValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"city", "addressValidator.wymagane");
		
		if (errors.getErrorCount() == 0) 
		{
			if (StringUtils.hasText(address.getPostCode()) && postCodeValidator.isValid(address.getPostCode()) == false)
			{
				errors.rejectValue("postCode", "addressValidator.postCode.invalid");
			}
			if (StringUtils.hasText(address.getFlatNumber()) && flatNumberValidator.isValid(address.getFlatNumber()) == false)
			{
				errors.rejectValue("flatNumber", "addressValidator.flatNumber.invalid");
			}
			if (StringUtils.hasText(address.getHomeNumber()) && homeNumberValidator.isValid(address.getHomeNumber()) == false)
			{
				errors.rejectValue("homeNumber", "addressValidator.homeNumber.invalid");
			}
			
			
		} 
  }
}
