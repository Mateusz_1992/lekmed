package pl.AAMCH.lekmed.validators;
import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.AAMCH.lekmed.domain.User;

public class UserPasswordValidator implements Validator
{
	private static String passwordRegEx = new String("[A-Z]\\w{4,}");
	RegexValidator passwordValidator = new RegexValidator(passwordRegEx);

	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return User.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object arg0, Errors arg1)
	{
		// TODO Auto-generated method stub
	}
	
	public void validate(User user, Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors,"password", "userPasswordValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"passwordConfirm", "userPasswordValidator.wymagane");

		
		if (errors.getErrorCount() == 0) 
		{
			if (StringUtils.hasText(user.getPassword()) && StringUtils.hasText(user.getPasswordConfirm()) && !(user.getPasswordConfirm().equals(user.getPassword())))
			{
				errors.rejectValue("passwordConfirm", "userPasswordValidator.password.bad");
			}
			if (StringUtils.hasText(user.getPassword()) && passwordValidator.isValid(user.getPassword()) == false)
			{
				errors.rejectValue("password", "userPasswordValidator.password.invalid");
			}
		} 
  }
}
