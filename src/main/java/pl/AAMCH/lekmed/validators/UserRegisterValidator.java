package pl.AAMCH.lekmed.validators;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.AAMCH.lekmed.domain.User;

public class UserRegisterValidator implements Validator
{
	private static String telephoneRegEx = new String("\\+48-[0-9]{3}-[0-9]{3}-[0-9]{3}");
	RegexValidator phoneValidator = new RegexValidator(telephoneRegEx);
	  
	private static String peselRegEx = new String("[0-9]{11}");
	RegexValidator peselValidator = new RegexValidator(peselRegEx);
	
	private static String passwordRegEx = new String("[A-Z]\\w{4,}");
	RegexValidator passwordValidator = new RegexValidator(passwordRegEx);
	
	private static String dateRegEx = new String("[0-9]{4}-[0-9]{2}-[0-9]{2}");
	RegexValidator dateValidator = new RegexValidator(dateRegEx);

	
	EmailValidator emailValidator = EmailValidator.getInstance();
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return User.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object arg0, Errors arg1)
	{
		// TODO Auto-generated method stub
	}
	
	public void validate(User user, Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors,"name", "userRegisterValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"surname", "userRegisterValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"phoneNumber", "userRegisterValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"email", "userRegisterValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"login", "userRegisterValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"pesel", "userRegisterValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"dataUrodzenia", "userRegisterValidator.wymagane");
		
		if (errors.getErrorCount() == 0) 
		{
			if (StringUtils.hasText(user.getEmail()) && emailValidator.isValid(user.getEmail()) == false)
			{
				errors.rejectValue("email", "userRegisterValidator.email.invalid");
			}
			if (StringUtils.hasText(user.getPhoneNumber()) && phoneValidator.isValid(user.getPhoneNumber()) == false)
			{
				errors.rejectValue("phoneNumber", "userRegisterValidator.phoneNumber.invalid");
			}
			if (StringUtils.hasText(user.getPassword()) && StringUtils.hasText(user.getPasswordConfirm()) && !(user.getPasswordConfirm().equals(user.getPassword())))
			{
				errors.rejectValue("passwordConfirm", "userRegisterValidator.password.bad");
			}
			if (StringUtils.hasText(user.getPesel()) && peselValidator.isValid(user.getPesel()) == false)
			{
				errors.rejectValue("pesel", "userRegisterValidator.pesel.invalid");
			}
			if (StringUtils.hasText(user.getPassword()) && passwordValidator.isValid(user.getPassword()) == false)
			{
				errors.rejectValue("password", "userRegisterValidator.password.invalid");
			}
			if (StringUtils.hasText(user.getDataUrodzenia()) && dateValidator.isValid(user.getDataUrodzenia()) == false)
			{
				errors.rejectValue("dataUrodzenia", "userRegisterValidator.date.invalid");
			}
			
			String dataString, peselString, rokD, miesiacD, dzienD, rokP, miesiacP, dzienP;

			
			
			if(user.getDataUrodzenia() != null)
			{
				 dataString = user.getDataUrodzenia();
				 rokD = dataString.substring(2, 4);
				 miesiacD = dataString.substring(5, 7);
				 dzienD = dataString.substring(8, 10);
			}
			else
			{
				 dataString = new String("");
				 rokD = new String("");
				 miesiacD = new String("");
				 dzienD = new String("");
			}
			if(!user.getPesel().isEmpty())
			{
				peselString = user.getPesel();
				rokP = peselString.substring(0, 2);
				miesiacP = peselString.substring(2, 4);
				dzienP = peselString.substring(4, 6);
			}
			else
			{
				rokP = new String("0");
			 	miesiacP = new String("0");
			 	dzienP = new String("0");
			}
			if(!rokD.equals(rokP) || !miesiacD.equals(miesiacP) || !dzienD.equals(dzienP))
			{
				errors.rejectValue("dataUrodzenia", "userRegisterValidator.date.bad");
			}
		} 
  }
}
