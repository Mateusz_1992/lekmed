package pl.AAMCH.lekmed.validators;

import java.util.List;

import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.AAMCH.lekmed.domain.Appointment;
import pl.AAMCH.lekmed.domain.Schedule;

public class AppoitmentValidator implements Validator
{
	
	private static String dateRegEx = new String("[0-9]{4}-[0-9]{2}-[0-9]{2}");
	RegexValidator dateValidator = new RegexValidator(dateRegEx);
	
	
	@Override
	public boolean supports(Class<?> clazz) 
	{
		return Appointment.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object arg0, Errors arg1) 
	{
	
	}

	public void validate(Appointment appointment, Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors,"date", "appoitmentValidator.wymagane");
		ValidationUtils.rejectIfEmpty(errors,"hour", "appoitmentValidator.wymagane");
		
		if (StringUtils.hasText(appointment.getDate()) && dateValidator.isValid(appointment.getDate()) == false)
		{
			errors.rejectValue("date", "appoitmentValidator.date.invalid");
		}
		
		boolean brakData = true;
		boolean brakGodzina = true;

		List<Schedule> scheduleList = appointment.getUser().getSchedule();							
		for (int i = 0; i < scheduleList.size(); i++) 
		{
			int sSh = 0;
			int sEh = 0;
			int aH = 0;
			if(scheduleList.get(i).getDate().toString().equals(appointment.getDate()))
			{
				brakData = false;
				sSh = Integer.valueOf(scheduleList.get(i).getStartHour().substring(0, 2));
				sEh = Integer.valueOf(scheduleList.get(i).getEndHour().substring(0, 2));
				aH = Integer.valueOf(appointment.getHour().substring(0, 2));
				
				if((sSh <= aH) && (sEh > aH))
				{
					brakGodzina =  false;
				}
			}
		}
		

			if(StringUtils.hasText(appointment.getDate()) && brakData)
			{
				errors.rejectValue("date", "appoitmentValidator.date.bad");
			}
			if(StringUtils.hasText(appointment.getHour()) && brakGodzina)
			{
				errors.rejectValue("hour", "appoitmentValidator.hour.bad");
			}
	}
}
