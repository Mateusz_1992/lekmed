package pl.AAMCH.lekmed.controllers;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.AAMCH.lekmed.domain.Appointment;
import pl.AAMCH.lekmed.domain.Receipt;
import pl.AAMCH.lekmed.service.AppointmentService;
import pl.AAMCH.lekmed.service.ReceiptService;
import pl.AAMCH.lekmed.service.RoomService;
import pl.AAMCH.lekmed.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ClientController {
	
	private static final Logger logger = LoggerFactory.getLogger(ClientController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	ReceiptService receiptService;
	
	@Autowired
	AppointmentService appointmentService;
	
	
	@RequestMapping(value="/clientRachunki")
	public String clientListaWizyt(Map<String, Object> map, HttpServletRequest request) 
	{   
		
		int userId = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		List<Receipt> listaRachunkow = receiptService.listReceipt();
		
		for (int i = 0; i < listaRachunkow.size(); i++) 
		{
			if(listaRachunkow.get(i).getAppointment().getPatient().getId() != userId)
			{
				listaRachunkow.remove(i);
			}
		}
		
		map.put("receipList", listaRachunkow);
		
		
	    return "clientRachunki";
	}
	
	@RequestMapping(value="/clientMojeWizyty")
	public String clientMojeWizyty(Map<String, Object> map, HttpServletRequest request) 
	{   
		int userId = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		List<Appointment> listaWizyt = appointmentService.listAppointment();
		
		for (int i = 0; i < listaWizyt.size(); i++) 
		{
			if(listaWizyt.get(i).getPatient().getId() != userId)
			{
				listaWizyt.remove(i);
			}
		}
		
		map.put("appointmentList", listaWizyt);
		
		
	    return "clientMojeWizyty";
	}
	
	@RequestMapping(value="/clientTerminarz")
	public String clientTerminarz(Map<String, Object> map, HttpServletRequest request) 
	{   
		
		map.put("list", userService.listUser());
	    return "clientTerminarz";
	}
}
