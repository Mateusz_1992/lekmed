package pl.AAMCH.lekmed.controllers;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import java.util.UUID;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.AAMCH.lekmed.domain.Address;
import pl.AAMCH.lekmed.domain.Receipt;
import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.service.AddressService;
import pl.AAMCH.lekmed.service.ReceiptService;
import pl.AAMCH.lekmed.service.UserService;
import pl.AAMCH.lekmed.utils.MailMail;
import pl.AAMCH.lekmed.validators.UserRegisterValidator;
import pl.AAMCH.lekmed.validators.AddressValidator;
import pl.AAMCH.lekmed.validators.EditUserValidator;

@Controller
public class HomeController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AddressService addressService;
	
	@Autowired
	MailMail mailMail;
	
	@Autowired
	ReCaptchaImpl reCaptcha;
	
	@Autowired
	ReceiptService receiptService;
	
	UserRegisterValidator userRegisterValidator = new UserRegisterValidator();
	EditUserValidator editUserValidator = new EditUserValidator();
	AddressValidator addressValidator = new AddressValidator();
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
    @RequestMapping("/hello")
    public ModelAndView helloWorld() {
 
        String message = "Hello World!";
        return new ModelAndView("hello", "message", message);
    }
    
    @RequestMapping("/error403")
    public ModelAndView error403() {
 
        String message = "Error403";
        return new ModelAndView("Error403", "Error403", message);
    }
	
    @RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {
 
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
 
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");
 
		return model;
	}
    

    @RequestMapping(value = "/rejestracja", method = RequestMethod.GET)
    public ModelAndView zarejestruj()
    {
    	ModelAndView model = new ModelAndView();
        model.addObject("userForm", new User());

        return model;
    }
    
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String zarejestruj(@ModelAttribute("userForm") User userForm,ServletRequest servletRequest, BindingResult bindingResult, Model model, @RequestParam("recaptcha_challenge_field") String challangeField, @RequestParam("recaptcha_response_field") String responseField) 
    {
    	boolean wyslany = false;
    	userRegisterValidator.validate(userForm, bindingResult);

    	String remoteAddress = servletRequest.getRemoteAddr();
		ReCaptchaResponse reCaptchaResponse = this.reCaptcha.checkAnswer(remoteAddress, challangeField, responseField);
    	
        if (!bindingResult.hasErrors() && reCaptchaResponse.isValid()) 
        {
        	try {
	       		Address addressTemp = new Address();
	       		addressTemp.setStreet("Strret"+userForm.getId());
	       		addressTemp.setHomeNumber("HomeNumber"+userForm.getId());
	       		addressTemp.setFlatNumber("FlatNumber"+userForm.getId());
	       		addressTemp.setPostCode("PostCode"+userForm.getId());
	       		addressTemp.setCity("City"+userForm.getId());
	       		addressService.addAddress(addressTemp);
	       		
	            userForm.setAddress(addressService.getAddress(addressTemp.getId()));
	            userForm.setActive(false);
	            userForm.setTokenActivate(UUID.randomUUID().toString());
	            userService.addUser(userForm);
	       		mailMail.sendMail("lekmed2016@gmail.com",
	          		   userForm.getEmail(),
	          		   "Aktywacja",
	          		   "Prosze kliknac ponizszy link: \n" + " http://localhost:8080/lekmed/aktywacja?userId=" + userForm.getId() +"&token=" + userForm.getTokenActivate());
	       				wyslany = true;
	             
			} catch (Exception e) 
	       	{
				System.out.println(e.toString());
			}
        }
        else
        {
        	
			model.addAttribute("invalidRecaptcha", true);
        	return "rejestracja";
        }
	       	
	       	
       	if(wyslany)
       	{
       		return "redirect:/pomyslnaRejestracja.html";
       	}
       	else
       	{
       		return "redirect:/blednaRejestracja.html";
        }
    }
    
    
    @RequestMapping("/pomyslnaRejestracja")
    public ModelAndView pomyslnaRejestracja() {
 
        boolean message = true;
        return new ModelAndView("pomyslnaRejestracja", "message", message);
    }
    
    @RequestMapping("/blednaRejestracja")
    public ModelAndView blednaRejestracja() {
 
        boolean message = true;
        return new ModelAndView("blednaRejestracja", "message", message);
    }
    
    @RequestMapping(value="/aktywacja")
	public String aktywacja(Map<String, Object> map, HttpServletRequest request) 
	{   User user;
		int userId = ServletRequestUtils.getIntParameter(request, "userId" , -1);
		String token = ServletRequestUtils.getStringParameter(request, "token" , " ");
		
		try {
			user = userService.getUser(userId);
		} catch (Exception e) {
			System.out.println(e.toString());
			return "redirect:/aktywacjaBlad.html";
		}
		
		if(user.equals(null))
		{
			return "redirect:/aktywacjaBlad.html";
		}
		if(user.getTokenActivate().equals(token))
		{
			user.setActive(true);
			userService.editUser(user);
			return "redirect:/pomyslnaAktywacja.html";
		}
		else
		{
			return "redirect:/aktywacjaBlad.html";
		}
		
		
	}
    
    @RequestMapping("/pomyslnaAktywacja")
    public ModelAndView pomyslnaAktywacja() {
 
        boolean message = true;
        return new ModelAndView("pomyslnaAktywacja", "message", message);
    }
    
    @RequestMapping("/aktywacjaBlad")
    public ModelAndView aktywacjaBlad() {
 
        boolean message = true;
        return new ModelAndView("aktywacjaBlad", "message", message);
    }
    
    @RequestMapping(value = "/edytujKonto", method=RequestMethod.GET)
	public String edytujKontoForm(Map<String, Object> map, HttpServletRequest request)
    {
    	User user = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        
    	if(user != null)   	
    	{
    		map.put("edit", user);
     	    map.put("selectedAddress", userService.getUser(userService.getUser(user.getId()).getId()).getAddress().getId());
 		  
    	}
    	
    	 map.put("userRoleList", userService.listUserRole());
	     map.put("addressesList", addressService.listAddress());
    	
    	   
		   return "edytujKonto";
	}
    
    //--------Edycja danych o uzytkowniku przez niego samego--------\\
    @RequestMapping(value = "/editUser", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("edit") User user, BindingResult result, HttpServletRequest request, Map<String, Object> map)
    {     
    	try 
    	{
    		editUserValidator.validate(user, result);
    	 	   
        	if(result.getErrorCount() == 0)
        	{																// nie edytowane dane zostaja przepisane z urzytkownika o tym samym id z przed edycji
        	  User oldUser = userService.getUser(user.getId());
     		  user.setUserRole(oldUser.getUserRole());
     		  user.setActive(oldUser.isActive());
     		  user.setRoom(oldUser.getRoom());
     		  user.setPassword(oldUser.getPassword());
     		  user.setAppointmentList(oldUser.getAppointmentList());
     		  user.setAppointment2List(oldUser.getAppointment2List());
     		  user.setAddress(oldUser.getAddress());
     		  user.setLogin(oldUser.getLogin());
     		  user.setSchedule(oldUser.getSchedule());
     		  user.setTokenActivate(oldUser.getTokenActivate());
     		  userService.editUser(user);
     		  map.put("success", true);
     		  return "edytujKonto";
        	}
        	else
        	{
        		return "edytujKonto";
        	}
		}
    	catch (Exception e) 
    	{
			map.put("bladEditUser", true);
			return "errorrOgolny";
		}
    	
    }
    
    @RequestMapping(value = "/edytujAdres", method=RequestMethod.GET)
	public String edytujAdresForm(Map<String, Object> map, HttpServletRequest request)
    {
    	User user = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
    	
    	if(user != null)
    	{
     	   map.put("editAdd", addressService.getAddress(userService.getUser(user.getId()).getAddress().getId()));
     	   map.put("selectedAddress", userService.getUser(userService.getUser(user.getId()).getAddress().getId()));
 		  
    	}

    	   
		return "edytujAdres";
		
		
	}
    
    @RequestMapping(value = "/editAddress", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("editAdd") Address address, BindingResult result, HttpServletRequest request, Map<String, Object> map) 
    {	
 	  try 
 	  {
 		  addressValidator.validate(address, result);
 	 	  if(result.getErrorCount()==0)
 	 	  {
 	 		  addressService.editAddress(address);
 	 		  map.put("success", true);
 	 		  return "edytujAdres";
 	 	  }
 	 	  else
 	 	  {
 	 		  return "edytujAdres";
 	 	  }
 	  }
 	  catch (Exception e)
 	  {
		map.put("bladEditAddres", true);
		return "errorOgolny";
 	  }
    }
    
    
    @RequestMapping(value = "/generatePdf/{receiptId}", method = RequestMethod.GET)
    public ModelAndView downloadExcel(@PathVariable("receiptId") Integer receiptId) 
	{
        Receipt rachunek = receiptService.getReceipt(receiptId);
    	
        // return a view which will be resolved by an excel view resolver
        return new ModelAndView("pdfView", "rachunek", rachunek);
    }
    
    
}
