package pl.AAMCH.lekmed.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import pl.AAMCH.lekmed.domain.Appointment;
import pl.AAMCH.lekmed.domain.Receipt;
import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.domain.Schedule;
import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.service.AppointmentService;
import pl.AAMCH.lekmed.service.ReceiptService;
import pl.AAMCH.lekmed.service.ScheduleService;
import pl.AAMCH.lekmed.service.UserService;

/**
 * Handles requests for the application home page.
 */

@Controller
@SessionAttributes
public class UserController {
	
	@Autowired
	UserService userService;

	@Autowired
	ScheduleService scheduleService;
	
	@Autowired 
	AppointmentService appointmentService;
	
	@Autowired
	ReceiptService receiptService;
	
	@RequestMapping(value="/userListaWizyt")
	public String userListaWizyt(Map<String, Object> map, HttpServletRequest request) 
	{
		
		int userId = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		List<Appointment> listawizyt = appointmentService.listAppointment();
		
		for (int i = 0; i < listawizyt.size(); i++) 
		{
			if(listawizyt.get(i).getUser().getId() != userId)
			{
				listawizyt.remove(i);
			}
		}
		
		map.put("userAppointmentList", listawizyt);

		return "userListaWizyt";
	}
	
	@RequestMapping(value="/userEdytujTerminarz")
	public String userEdytujTerminarzList(Map<String, Object> map, HttpServletRequest request) 
	{   
		User user = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        
		List<Schedule> listSchedule = userService.getUser(user.getId()).getSchedule();
		for (int i = 0; i < listSchedule.size(); i++) 
		{
			for (int j = 0; j < listSchedule.size(); j++) 
			{
				if(i!=j)
				{
					if(listSchedule.get(i).getId() == listSchedule.get(j).getId())
					{
						listSchedule.remove(j);
					}
				}
			}
		}	
		if(user != null)
		{
			map.put("listSchedule", listSchedule);
		}
	    return "userEdytujTerminarz";
	}
	
	@RequestMapping("/userDodajSchedule")
	public String userEdytujTerminarzForm(Map<String, Object> map, HttpServletRequest request) {
	   
		int scheduleId = ServletRequestUtils.getIntParameter(request, "scheduleId" , -1);
		   
		if (scheduleId > 0)
		{
			map.put("edit", scheduleService.getSchedule(scheduleId));
		}
	    else
	    {
		   map.put("edit", new Schedule());   
	    }
		
	    return "userDodajSchedule";
	}
	
	
	@RequestMapping(value = "/addSchedule", method = RequestMethod.POST)
    public String addSchedule(@ModelAttribute("edit") Schedule schedule, BindingResult result, HttpServletRequest request, Map<String, Object> map) 
	{
         
		User user = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
 	   
 		   if(result.getErrorCount()==0)
		   {
			   if (schedule.getId()==0)
			   {
				   scheduleService.addSchedule(schedule);
			   	   user.getSchedule().add(scheduleService.getSchedule(schedule.getId()));
			   	   userService.editUser(user);
			   }
		       else
				   scheduleService.editSchedule(schedule);
			   
			   return "redirect:userEdytujTerminarz.html";
	   	   }
 	   
    
 	   return "userDodajSchedule";
    }
	
	
	
	@RequestMapping("/userDeleteSchedule/{scheduleId}")
	   public String deleteAddress(@PathVariable("scheduleId") Integer scheduleId) 
	   {	
		User user = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		
			try	{
				for(int i = 0; i < user.getSchedule().size(); i++)
				{
					if(user.getSchedule().get(i).getId() == scheduleId)
					{
						user.getSchedule().remove(i);
					}
				}
			   user.setActive(userService.getUser(user.getId()).isActive());
			   user.setUserRole(userService.getUser(user.getId()).getUserRole());
			   userService.editUser(user);
			   scheduleService.removeSchedule(scheduleId);
		   	    } catch (Exception e) {
		   	    	System.out.println(e.toString());
		   	    }
	  

	       return "redirect:/userEdytujTerminarz.html";
	   }
	
	
	
	@RequestMapping(value="/userRachunki/{appointmentId}")
	public String addRachunek(@PathVariable("appointmentId") Integer appointmentId) 
	{   
		Receipt rachunek = new Receipt();
		
		rachunek.setCost(150);
		rachunek.setAppointment(appointmentService.getAppointment(appointmentId));
		
		receiptService.addReceipt(rachunek);
		
	    return "redirect:/userRachunki.html";
	}
	
	
	@RequestMapping(value="/userRachunki")
	public String userRachunki(Map<String, Object> map, HttpServletRequest request) 
	{   
		int userId = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		List<Receipt> listaRachunkow = receiptService.listReceipt();
		
		for (int i = 0; i < listaRachunkow.size(); i++) 
		{
			if(listaRachunkow.get(i).getAppointment().getUser().getId() != userId)
			{
				listaRachunkow.remove(i);
			}
		}
		
		map.put("receipList", listaRachunkow);
		
	    return "userRachunki";
	}
}
