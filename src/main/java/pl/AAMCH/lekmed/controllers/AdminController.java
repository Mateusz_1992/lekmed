package pl.AAMCH.lekmed.controllers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.AAMCH.lekmed.domain.Address;
import pl.AAMCH.lekmed.domain.Receipt;
import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.service.AddressService;
import pl.AAMCH.lekmed.service.AppointmentService;
import pl.AAMCH.lekmed.service.ReceiptService;
import pl.AAMCH.lekmed.service.RoomService;
import pl.AAMCH.lekmed.service.UserService;
import pl.AAMCH.lekmed.validators.AddressValidator;
import pl.AAMCH.lekmed.validators.UserPasswordValidator;
import pl.AAMCH.lekmed.validators.UserRegisterValidator;

/**
 * Handles requests for the application admin page.
 */
@Controller
public class AdminController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AddressService addressService;

	@Autowired
	AppointmentService appointmentService;
	
	@Autowired
	ReceiptService receiptService;
	
	@Autowired
	RoomService roomService;
	
	UserRegisterValidator userRegisterValidator = new UserRegisterValidator();
	UserPasswordValidator userPasswordValidator = new UserPasswordValidator();
	AddressValidator addressValidator = new AddressValidator();
	
	
	//*************************Zarz�dzanie u�ytkownikami*******************************\\
	
	//-------Widok z lista wszystkich u�ytkownik�w----------\\
	@RequestMapping(value="/adminListaUzytkownikow")
	public String listUsers(Map<String, Object> map, HttpServletRequest request) 
	{   
		try
		{
			map.put("userList", userService.listUser());											// przekazanie do widoku listy wszystkich uzytkownikow
		    return "adminListaUzytkownikow";
		}
		catch (Exception e) 
		{
			map.put("bladListaUzytkownikow", true);
			return "errrorOgolny"; 
		}
	}
	
	//--------Widok z formularzem edycji u�ytkownika / dodawania nowego u�ytkownika przez admina-------\\
	@RequestMapping("/adminDodajUzytkownika")
	public String userForm(Map<String, Object> map, HttpServletRequest request) 
	{   
		try 
		{
			
			int userId = ServletRequestUtils.getIntParameter(request, "userId" , -1);			// poranie przekazanego id uzytkownika do edycji z url przekazano jako parametr
			
			if (userId > 0)																		//je�eli podajemy userId > 0  czyli uzytkownika kory istnieje to bedziemy go edytowa�
			{																					
				int roomId;																		// jesli nie ma przypisanego pokoju do uzytkownika to roomId ustawiane spoza zakresu istniejacych
				if(userService.getUser(userId).getRoom() == null)
					roomId = -1;
				else
					roomId = userService.getUser(userId).getRoom().getId();						// jesli jest to przypisujemy odpowiednie id
				
				map.put("user", userService.getUser(userId));									// do widoku przekazywany jest uzytkownik ktrego chcemy edytowac
				map.put("selectedAddress", userService.getUser(userId).getAddress().getId());	// oraz id przypisanego do niego adresu w celu ustawienia odpowiedniego w widoku
				map.put("roomSelected", roomId);												// i to samo z pokojem jesli takowy jest przypisany je�li nie to id bedzie poza zakresem istniejacych
			}
		    else																				//jesli userId <=0 to bedzie tworzony nowy uzytkownik
		    {
			   map.put("user", new User());   													// i przekaujemy nowy obiekt encji User do wype�nienia go naszymi danymi
		    }
			
			map.put("addressesList", addressService.listAddress());								// przekazujemy do widoku listy wszystkich adres�w
			map.put("roomList", roomService.listRoom());										// liste mozliwych do wybrania pokoi specjalistycznych
			
		    return "adminDodajUzytkownika";
			
		} catch (Exception e) 
		{
			map.put("bladDodajUserAdmin", true);
			return "errorOgolny";
		}
	}
	
	//--------Edycja u�ytkownika / dodawanie nowego u�ytkownika przez admina-------\\
	@RequestMapping(value="/adminAddUser")
	public String adminAddUser(@ModelAttribute("user") User user, BindingResult result, HttpServletRequest request, Map<String, Object> map) 
	{
	   try 
	   {
		
		   userRegisterValidator.validate(user, result);  											// Walidacja czy dane wprowadzone do edycji / nowego uzytkownika s� poprawne bledy zapisuja sie w result
		   if(result.getErrorCount() == 0)
		   {
			   if (user.getId()==0)
			   {   
				   userService.adminAddUser(user);													// Jezeli id rowne 0 to dodajemy nowego uzytkownika
				   return "redirect:adminListaUzytkownikow.html";									// przekierowujemy do widoku listy wszystkich uzytkownikow
			   }
			   else
			   {   
				   user.setActive(userService.getUser(user.getId()).isActive());					// jezeli id > 0 to przepisujemy te informacje ktorych nie zminiamy
				   user.setUserRole(userService.getUser(user.getId()).getUserRole());				// na te co by�y juz zapisane w uzytkoniko o podanym id
				   user.setPassword(userService.getUser(user.getId()).getPassword());
				   user.setTokenActivate(userService.getUser(user.getId()).getTokenActivate());
				   userService.editUser(user);
				   return "redirect:adminListaUzytkownikow.html";									// jesli pomyslnie rowniez przekierowujemy na liste wszystkich uzytkownikow
			   }
		   }
		   else																						// jesli wystepuja bledy walidacji to zostajemy na tym samym
		   {																						// widoku i przekazujemy potrzebne do formularza listy
			  map.put("addressesList", addressService.listAddress());
			  map.put("roomList", roomService.listRoom());
			  return "adminDodajUzytkownika";															
		   } 
		   
	   } 
	   catch (Exception e) 
	   {
		   map.put("bladEdycjaUserAdmin", true);
		   return "errorOgolny";
	   }    
	}
	
	//--------Aktywacja / dezaktywacja u�ytkownika przez admina-------\\
	@RequestMapping("/adminActiveUzytkownik/{userId}")
	public String activeUser(@PathVariable("userId") Integer userId, Map<String, Object> map) 
	{	
		try 
		{
			User user = userService.getUser(userId);						// pobieramy uzytkownika ktrego id przekazujemy w url
			
			if(user.isActive())												// i jezeli by� aktywny to dezaktywujemy
			{
				user.setActive(false);
			}
			else															// i na odwrot
			{
				user.setActive(true);
			}
			
			userService.editUser(user);										// edytuje uzytkownika w bazie
			return "redirect:/adminListaUzytkownikow.html";					// i przekierowujemy nana liste wszystkich uzytkownikow
		}
		catch (Exception e)
		{
			map.put("bladAktywacjaUserAdmin", true);
			return "errorOgolny";
		}
	}
	
	//--------Aktywacja / dezaktywacja u�ytkownika przez admina-------\\
	@RequestMapping("/adminDeleteUzytkownik/{userId}")
	public String deleteUser(@PathVariable("userId") Integer userId, Map<String, Object> map)
	{
		try 
		{
			User user = userService.getUser(userId);													// pobranie uzytkownika ktory ma zostac usuniety
			Integer idAddress = user.getAddress().getId();											// ustalenie id adresu do usuniecia wraz z uzytkownikiem
			userService.removeUser(userId);															// usuniecie uzytkownika
			addressService.removeAddress(idAddress);													// usuniecie adresu

			return "redirect:/adminListaUzytkownikow.html";										    // przekierownie do widoku z lista wszystkich uzytkownikow 
		}
		catch (Exception e)
		{
			map.put("bladUsunieciaUzytkownika", true);
			return "bladOgolny";
		}
	}  
   
	//--------dodanie / usuniecie roli ADMIN u�ytkownika przez admina-------\\  
	@RequestMapping("/adminRoleAdmin/{userId}")
	public String changeRoleAdmin(@PathVariable("userId") Integer userId, Map<String, Object> map) 
	{	
		try
		{
			Boolean czyAdmin = false;
			Boolean czyUser = false;
			Boolean czyClient = false;
			
			User user = userService.getUser(userId);						// pobranie uzytkownika dla ktroego zminiamy status roli
			Set<Role> roleSet = user.getUserRole();							// set przyznanyc roli dla uzytkonika
			Role rola = null;
			Iterator<Role> iterator = roleSet.iterator();
			
			while(iterator.hasNext())										// sprawdzenie jakie role posiada uzytkownik
			{
				rola = (Role) iterator.next();
				if(rola.getRoleName().equals("ROLE_ADMIN"))   
					czyAdmin = true;
				if(rola.getRoleName().equals("ROLE_USER"))
					czyUser = true;
				if(rola.getRoleName().equals("ROLE_CLIENT"))
					czyClient = true;
			}
			czyAdmin= !czyAdmin;											// zmina statusu Roli ADMIN				
			
			Set<Role> roleSetNew = new HashSet<Role>(0);					// utworzenie nowego setu z rolami dla uzytkownika
			if(czyAdmin)
			{
				Role rA = (Role) userService.findRoleByName("ROLE_ADMIN");
				roleSetNew.add(rA);
			}
			if(czyUser)
			{
				Role rU = (Role) userService.findRoleByName("ROLE_USER");
				roleSetNew.add(rU);
			}
			if(czyClient)
			{
				Role rC = (Role) userService.findRoleByName("ROLE_CLIENT");
				roleSetNew.add(rC);
			}
			
			user.setUserRole(roleSetNew);
			user.setActive(userService.getUser(userId).isActive());
			userService.editUser(user);
	   
			return "redirect:/adminListaUzytkownikow.html";
		}
		catch
		(Exception e)
		{
			map.put("bladEditRolaAdmin", true);
			return "errorOgolny";
		}
	}
   
	//--------dodanie / usuniecie roli USER u�ytkownika przez admina-------\\ 
	@RequestMapping("/adminRoleUser/{userId}")
	public String changeRoleUser(@PathVariable("userId") Integer userId, Map<String, Object> map) 
	{	
		try
		{
			Boolean czyAdmin = false;
			Boolean czyUser = false;
			Boolean czyClient = false;
			User user = userService.getUser(userId);														// pobranie uzytkownika dla ktroego zminiamy status roli
			Set<Role> roleSet = user.getUserRole();															// set przyznanyc roli dla uzytkonika
			Role rola = null;
			Iterator<Role> iterator = roleSet.iterator();
			while(iterator.hasNext())																	// sprawdzenie jakie role posiada uzytkownik
			{
				rola = (Role) iterator.next();
				if(rola.getRoleName().equals("ROLE_ADMIN"))
					czyAdmin = true;
				if(rola.getRoleName().equals("ROLE_USER"))
					czyUser = true;
				if(rola.getRoleName().equals("ROLE_CLIENT"))
					czyClient = true;
			}
			
			czyUser= !czyUser;																			// zmina statusu Roli USER	
			
			Set<Role> roleSetNew = new HashSet<Role>(0);												// utworzenie nowego setu z rolami dla uzytkownika
			if(czyAdmin)
			{
				Role rA = (Role) userService.findRoleByName("ROLE_ADMIN");
				roleSetNew.add(rA);
			}
			if(czyUser)
			{
				Role rU = (Role) userService.findRoleByName("ROLE_USER");
				roleSetNew.add(rU);
			}
			if(czyClient)
			{
				Role rC = (Role) userService.findRoleByName("ROLE_CLIENT");
				roleSetNew.add(rC);
			}
	   
			user.setUserRole(roleSetNew);																// ustawienie newego setu dla uzytkownika i edycja w bazie
			user.setActive(userService.getUser(userId).isActive());
			userService.editUser(user);
	   
			return "redirect:/adminListaUzytkownikow.html";
		}
		catch (Exception e)
		{
			map.put("bladEditRolaUser", true);
			return "errorOgolny";
		}
	}
   
	//--------dodanie / usuniecie roli CLIENT u�ytkownika przez admina-------\\ 
	@RequestMapping("/adminRoleClient/{userId}")
	public String changeRoleClient(@PathVariable("userId") Integer userId, Map<String, Object> map) 
	{	
		Boolean czyAdmin = false;
		Boolean czyUser = false;
		Boolean czyClient = false;
		User user = userService.getUser(userId);												// pobranie uzytkownika dla ktroego zminiamy status roli
		Set<Role> roleSet = user.getUserRole();													// set przyznanyc roli dla uzytkonika
		Role rola = null;
		Iterator<Role> iterator = roleSet.iterator();
		while(iterator.hasNext())																// sprawdzenie jakie role posiada uzytkownik
		{
			rola = (Role) iterator.next();
			if(rola.getRoleName().equals("ROLE_ADMIN"))   
				czyAdmin = true;
			if(rola.getRoleName().equals("ROLE_USER"))
				czyUser = true;
			else if(rola.getRoleName().equals("ROLE_CLIENT"))
				czyClient = true;
		}
		
		czyClient= !czyClient;																	// zmina statusu Roli CLIENT
		
		Set<Role> roleSetNew = new HashSet<Role>(0);											// utworzenie nowego setu z rolami dla uzytkownika
		if(czyAdmin)
		{
			Role rA = (Role) userService.findRoleByName("ROLE_ADMIN");
			roleSetNew.add(rA);
		}
		if(czyUser)
		{
			Role rU = (Role) userService.findRoleByName("ROLE_USER");
			roleSetNew.add(rU);
		}
		if(czyClient)
		{
			Role rC = (Role) userService.findRoleByName("ROLE_CLIENT");
			roleSetNew.add(rC);
		}
   
		user.setUserRole(roleSetNew);															// ustawienie newego setu dla uzytkownika i edycja w bazie
		user.setActive(userService.getUser(userId).isActive());
		userService.editUser(user);
		
		return "redirect:/adminListaUzytkownikow.html";
   }
   
	//--------widok zmiany has�a przez admina-------\\
	@RequestMapping("/adminEdytujHaslo/{userId}")
	public String adminEdytujHasloForm(@PathVariable("userId") Integer userId, Map<String, Object> map, HttpServletRequest request) 
	{	   
		try
		{
			if (userId > 0)
			{
				map.put("edit", userService.getUser(userId));			// przekazanie do widoku uzytkownika ktremu bedzie zmieniane haslo
			}
		    return "adminEdytujHaslo";
		}
		catch (Exception e)
		{
			map.put("bladEditHasloForm", true);
			return "errorOgolny";
		}
	}
   
	//--------zmianya has�a przez admina-------\\
	@RequestMapping(value = "/adminEditPassword", method = RequestMethod.POST)
	public String editPassword(@ModelAttribute("edit") User user, BindingResult result, HttpServletRequest request, Map<String, Object> map) 
	{
	   	try
	   	{
	   		userPasswordValidator.validate(user, result);
		   	if(result.getErrorCount() == 0)
		   	{
		   		User userek = userService.getUser(user.getId());
			   	user.setAddress(userek.getAddress());
			   	user.setUserRole(userek.getUserRole());
			   	user.setActive(userek.isActive());
			   	user.setTokenActivate(userek.getTokenActivate());
			   	user.setRoom(userek.getRoom());
			   	user.setDataUrodzenia(userek.getDataUrodzenia());
			   	user.setEmail(userek.getEmail());
			   	user.setName(userek.getName());
			   	user.setSurname(userek.getSurname());
			   	user.setLogin(userek.getLogin());
			   	user.setAppointmentList(userek.getAppointmentList());
			   	user.setAppointment2List(userek.getAppointment2List());
			   	user.setPesel(userek.getPesel());
			   	user.setPhoneNumber(userek.getPhoneNumber());
			   	user.setPassword(user.getPassword());
			   	user.setSchedule(userek.getSchedule());
				userService.editUserPassword(user);
				
				return "redirect:adminListaUzytkownikow.html";
		   	}
		   	else
		   	{
		   		return "adminEdytujHaslo";
		   	}
		}
	   	catch (Exception e)
	   	{
	   		map.put("bladEditHaslo", true);
			return "errorOgolny";
		}		   
   }
	
	
	//*************************Zarz�dzanie adresami*******************************\\
   
	//-------Widok z lista wszystkich adresow----------\\
	@RequestMapping(value="/adminListaAdresow")
	public String listaAdresow(Map<String, Object> map, HttpServletRequest request) 
	{   
		try
		{
			map.put("addressesList", addressService.listAddress());								// przekazanie do widoku listy wszystkich adresow
		    return "adminListaAdresow";
		}
		catch (Exception e)
		{
			map.put("bladListaAdresow", true);
			return "errorOgolny";
		}
	}

	//-------Widok dodanie / edycja adresu przez admina----------\\
	@RequestMapping("/adminDodajAdres")
	public String addressForm(Map<String, Object> map, HttpServletRequest request)
	{   
		try
		{
			int addressId = ServletRequestUtils.getIntParameter(request, "addressId" , -1);    // pobranie z parrametu url id adresu do edycji
			   
			if (addressId > 0)																   // jesli id > 0 edytujemy adres
				map.put("address", addressService.getAddress(addressId));
		    else
			   map.put("address", new Address());   											// jesli nie to bedzie tworzony nowy adres
		       
		    return "adminDodajAdres";
		}
		catch (Exception e)
		{
			map.put("bladDodajAdresFormAdmin", true);
			return "errorOgolny";
		}
	    
	    
	}
   
	//-------dodanie / edycja adresu przez admina----------\\
	@RequestMapping(value = "/adminAddAddress", method = RequestMethod.POST)
   	public String addContact(@ModelAttribute("address") Address address, BindingResult result, HttpServletRequest request, Map<String, Object> map)
	{    
		try
		{
			addressValidator.validate(address, result);							// walidacja adresu
			if(result.getErrorCount()==0)
			{
				if(address.getId()==0)											// jesli brak bledow to dodajemy lub edytujemy adres
					addressService.addAddress(address);
				else
					addressService.editAddress(address);
			   
				map.put("addressesList", addressService.listAddress());
				return "redirect:adminListaAdresow.html";						// przekierowanie na liste adresow
	   	   	}
			else
			{
				return "adminDodajAdres";										// jesli sa bledy walidacji to zostajemy na tym widoku
			}
		}
		catch (Exception e)
		{
			map.put("bladDodajAdresAdmin", true);
			return "errorOgolny";
		} 
	}
	
	//-------usuniecie adresu przez admina----------\\
	@RequestMapping("/adminDeleteAddress/{addressId}")
   	public String deleteAddress(@PathVariable("addressId") Integer addressId, Map<String, Object> map) 
	{	
		try 
		{
			addressService.removeAddress(addressId);				// usuniecie adresu z bazy
			return "redirect:/adminListaAdresow.html";
		}
		catch (Exception e)
		{
			map.put("bladDelAdresAdmin", true);
			return "errorOgolny";
		}  
	}
   
   
   	//*************************wizyty i rachunki*******************************\\
	
	//-----------------------tworzenie rachunku przez admina-----------------------------\\
   	@RequestMapping(value="/adminRachunki/{appointmentId}")
	public String addRachunekAdmin(@PathVariable("appointmentId") Integer appointmentId, Map<String, Object> map) 
	{   
		try
		{
			Receipt rachunek = new Receipt();
			rachunek.setCost(150);															//ustawienie ceny wizyty na sztywno
			rachunek.setAppointment(appointmentService.getAppointment(appointmentId));		// dodanie id wizyty ktorej dotyczy rachunek
			receiptService.addReceipt(rachunek);											// stworzenie w bazie nowego rachunku
		    return "redirect:/adminListaWizyt.html";
		}
		catch (Exception e)
		{
			map.put("bladRachunekAdmin", true);
		    return "errorOgolny";
		}
	}
}
