package pl.AAMCH.lekmed.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.AAMCH.lekmed.domain.Appointment;
import pl.AAMCH.lekmed.domain.QuadaraticEquation;
import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.domain.Schedule;
import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.service.QuadraticEquationService;

@Controller
public class QuadraticEquationController {
	@Autowired
	QuadraticEquationService quadraticEquationService; 
	
	
	@RequestMapping(value = "/quadraticEquation", method = RequestMethod.GET)
    public ModelAndView newEquation()
    {
    	ModelAndView model = new ModelAndView();
        model.addObject("equation", new QuadaraticEquation());

        return model;
    }
    
    @RequestMapping(value = "/count", method = RequestMethod.POST)
    public String countEquation2(@ModelAttribute("equation") QuadaraticEquation equation,
    							BindingResult bindingResult, 
								Model model)
    {
    	QuadaraticEquation e = equation;
    	List<QuadaraticEquation> listOfEquations = new ArrayList<QuadaraticEquation>();
    	
    	
    	if(e.getCoefficientA() != 0)
    	{
    		double tempDelta = Math.pow(e.getCoefficientB(), 2) - 4.0 * e.getCoefficientA() * e.getCoefficientC();
			e.setDelta(tempDelta);
    		if(tempDelta >= 0)
    		{
    			if(tempDelta > 0)
    			{
    				double solutionI = ((-1)*e.getCoefficientB() - Math.sqrt(tempDelta))/(2.0 * e.getCoefficientA());
    				double solutionII = ((-1)*e.getCoefficientB() + Math.sqrt(tempDelta))/(2.0 * e.getCoefficientA());
    				double [] tempSols = new double[2];
    				tempSols[0] = solutionI;
    				tempSols[1] = solutionII;
    				e.setSolutions(tempSols);
    				
    				e.setSolutionI(solutionI);
    				e.setSolutionII(solutionII);
    			}
    			else
    			{
    				double solution0 = ((-1)*e.getCoefficientB())/(2.0 * e.getCoefficientA());
    				double [] tempSols = new double[1];
    				tempSols[0] = solution0;
    				e.setSolutions(tempSols);
    				
    				e.setSolutionI(solution0);
    			}
    		}
    		else
    		{
        		System.out.println("Wyznacznik rownania kwadratowego mniejszy niz 0");	
    		}
			listOfEquations.add(e);
			model.addAttribute("equations", listOfEquations);
    	}
    	else
    	{
    		System.out.println("R�wnanie nie jest kwadratowe");
    	}
    	String d = "dupa";
    	return "count";
    } 
	
	
    @RequestMapping(value = "/dodajWspolczynniki", method = RequestMethod.GET)
    public ModelAndView addCoefficients()
    {
    	ModelAndView model = new ModelAndView();
        model.addObject("newEquation", new QuadaraticEquation());

        return model;
    }
    
    @RequestMapping(value = "/obliczonyWspolczynnik", method = RequestMethod.POST)
    public String countEquation(@ModelAttribute("newEquation") QuadaraticEquation newEquation, BindingResult bindingResult, Model model) 
    {

    	QuadaraticEquation  quadraticEquation = quadraticEquationService.getQuadaraticEquation(newEquation.getId());
    	
    	if(quadraticEquation.getCoefficientA() != 0)
    	{
    		double delta = quadraticEquation.getCoefficientB() * quadraticEquation.getCoefficientB()  - 4.0 * quadraticEquation.getCoefficientA() * quadraticEquation.getCoefficientC();
    	}
    	else
    	{
    		System.out.println("B��d");
    	}
    	

    	return "redirect:/obliczonyWspolczynnik.html";
    }
}
