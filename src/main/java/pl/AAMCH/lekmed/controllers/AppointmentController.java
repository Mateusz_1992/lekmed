package pl.AAMCH.lekmed.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import pl.AAMCH.lekmed.domain.Appointment;
import pl.AAMCH.lekmed.domain.Role;
import pl.AAMCH.lekmed.domain.User;
import pl.AAMCH.lekmed.service.AppointmentService;
import pl.AAMCH.lekmed.service.UserService;
import pl.AAMCH.lekmed.validators.AppoitmentValidator;



@Controller
@SessionAttributes
public class AppointmentController 
{
	
	@Autowired 
	AppointmentService appointmentService;
	
	@Autowired
	UserService userService;
		
	AppoitmentValidator appoitmentValidator = new AppoitmentValidator();
	
	//*************************wizyty*******************************\\
	   
   	//-----------------------widok z lista wszystkich wizyt-----------------------------\\
   	@RequestMapping("/adminListaWizyt")
	public String listAppointments(Map<String, Object> map, HttpServletRequest request)
	{
   		try 
   		{
   			map.put("appointmentList", appointmentService.listAppointment());		//przekazanie listy wszystkich wizyt do widoku
   			return "adminListaWizyt";
		}
   		catch (Exception e)
   		{
			map.put("bladListaWizytAdmin", true);
			return "errorOgolny";
		}
	}

   	
   	//-----------------------widok do tworzenia nowej wizyty-----------------------------\\
    @RequestMapping(value = "/clientDodajWizyte", method = RequestMethod.GET)
    public ModelAndView createNewAppointment()
    {
    	List<User> listaUsers = userService.listUser();
    	List<User> listaLekarzy = new ArrayList<User>();
		
    	
    	for (int i = 0; i < listaUsers.size(); i++)
    	{
	    	Set<Role> setRole = listaUsers.get(i).getUserRole();
	    	Iterator<Role> iterator = setRole.iterator();
	    	Role rola;
	    	while(iterator.hasNext())
			{
				rola = (Role) iterator.next();
				if(rola.getRoleName().equals("ROLE_USER"))
				{
					listaLekarzy.add(listaUsers.get(i));
				}
			}
    	}
    	
    	ModelAndView model = new ModelAndView();
        model.addObject("newAppointment", new Appointment());
        model.addObject("listaLekarzy", listaLekarzy);

        return model;
    }
    
    @RequestMapping(value = "/nowaWizyta", method = RequestMethod.POST)
    public String createNewAppointment(@ModelAttribute("newAppointment") Appointment newAppointment, BindingResult bindingResult, Map<String, Object> map) 
    {
    	
    	int patientId = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).getId();

    	appoitmentValidator.validate(newAppointment, bindingResult);
    	if(bindingResult.getErrorCount() <= 1)
    	{
	    	if(newAppointment.getId() == 0)
	    	{
	    		newAppointment.setPatient(userService.getUser(patientId));
	    		appointmentService.addAppointment(newAppointment);
	    		return "redirect:/clientMojeWizyty.html";
	    	}
	    	else
	    	{
	    		return "redirect:/clientMojeWizyty.html";
	    	}
    	}
    	else
    	{
    		List<User> listaUsers = userService.listUser();
    		List<User> listaLekarzy = new ArrayList<User>();
        	for (int i = 0; i < listaUsers.size(); i++)
        	{
    	    	Set<Role> setRole = listaUsers.get(i).getUserRole();
    	    	Iterator<Role> iterator = setRole.iterator();
    	    	Role rola;
    	    	while(iterator.hasNext())
    			{
    				rola = (Role) iterator.next();
    				if(rola.getRoleName().equals("ROLE_USER"))
    				{
    					listaLekarzy.add(listaUsers.get(i));
    				}
    			}
        	}
        	
        	map.put("listaLekarzy", listaLekarzy);
    		return "clientDodajWizyte";
    	}
    }
}
