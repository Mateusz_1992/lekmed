package pl.AAMCH.lekmed.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="User")
public class User 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="name", nullable=false)
	private String name;
	
	@Column(name="surname", nullable=false)
	private String surname;
	
	@Column(name="email", nullable=false, unique=true)
	private String email;
	
	@ManyToOne
	private Address address;
	
	@ManyToOne
	private Room room;
	
	@Column(name="pesel", nullable=false, unique=true)
	private String pesel;
	
	@Column(name="phoneNumber", nullable=false)
	private String phoneNumber;
	
	private boolean isActive;
	
	@Column(name="login", nullable=false, unique=true)
	private String login;
	
	@Column(name="password", nullable=false, unique=true)
	private String password;
	
	@Column(name="tokenActivate", nullable=false)
	private String tokenActivate;
	
	@Column(name="dataUrodzenia", nullable=false)
	private String dataUrodzenia;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Role> userRole = new HashSet<Role>(0);
	
	@Transient
    private String passwordConfirm;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
	private List<Schedule> schedule = new ArrayList<Schedule>(0);
	
	@OneToMany(mappedBy="user")
	private List<Appointment> appointmentList;
	
	@OneToMany(mappedBy="user")
	private List<Appointment> appointment2List;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getDataUrodzenia() {
		return dataUrodzenia;
	}
	public void setDataUrodzenia(String dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getTokenActivate() {
		return tokenActivate;
	}
	public void setTokenActivate(String tokenActivate) {
		this.tokenActivate = tokenActivate;
	}
	
	public Set<Role> getUserRole() {
		return userRole;
	}
	public void setUserRole(Set<Role> userRole) {
		this.userRole = userRole;
	}
	public String getPasswordConfirm() {
		return passwordConfirm;
	}
	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}
	public List<Schedule> getSchedule() {
		return schedule;
	}
	public void setSchedule(List<Schedule> schedule) {
		this.schedule = schedule;
	}
	public List<Appointment> getAppointmentList() {
		return appointmentList;
	}
	public void setAppointmentList(List<Appointment> appointmentList) {
		this.appointmentList = appointmentList;
	}
	public List<Appointment> getAppointment2List() {
		return appointment2List;
	}
	public void setAppointment2List(List<Appointment> appointment2List) {
		this.appointment2List = appointment2List;
	}
	
	
	
	
}
