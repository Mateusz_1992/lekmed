package pl.AAMCH.lekmed.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="appointment")
public class Appointment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;
	
	private String date;
	private String hour;

	@ManyToOne
	private User user;
	
	@ManyToOne
	private User patient;
	
	@OneToMany(mappedBy="appointment")
	private List<Receipt> receiptList;
	
	public Appointment()
	{
		;
	}
	

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	
	public List<Receipt> getReceiptList() {
		return receiptList;
	}



	public void setReceiptList(List<Receipt> receiptList) {
		this.receiptList = receiptList;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}



	public User getPatient() {
		return patient;
	}



	public void setPatient(User patient) {
		this.patient = patient;
	}
	
	
}
