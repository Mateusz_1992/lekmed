package pl.AAMCH.lekmed.domain;

public class QuadaraticEquation 
{		
	private int id;
	
	private double coefficientA;
	private double coefficientB;
	private double coefficientC;
	
	private double delta;
	private double solutions[];
	private double solutionI;
	private double solutionII;
	
	public QuadaraticEquation(){}
	
	public QuadaraticEquation(double coefficientA, double coefficientB, double coefficientC)
	{
		this.coefficientA = coefficientA;
		this.coefficientB = coefficientB;
		this.coefficientC = coefficientC;
	}
	
	public void countDelta()
	{
		delta = Math.pow(coefficientB, 2) - 4 * coefficientA * coefficientC;
	}
	
	public void countSolutions()
	{
		if(delta >= 0)
		{
			if(delta == 0)
			{
				solutionI = (((-1) * coefficientB))/(2 * coefficientA);		
			}
			else
			{
				solutionI = (((-1) * coefficientB) - Math.sqrt(delta))/(2 * coefficientA);
				solutionII = (((-1) * coefficientB) + Math.sqrt(delta))/(2 * coefficientA);
			}
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getCoefficientA() {
		return coefficientA;
	}
	public void setCoefficientA(double coefficientA) {
		this.coefficientA = coefficientA;
	}
	public double getCoefficientB() {
		return coefficientB;
	}
	public void setCoefficientB(double coefficientB) {
		this.coefficientB = coefficientB;
	}
	public double getCoefficientC() {
		return coefficientC;
	}
	public void setCoefficientC(double coefficientC) {
		this.coefficientC = coefficientC;
	}
	public double getDelta() {
		return delta;
	}
	public void setDelta(double delta) {
		this.delta = delta;
	}
	public double[] getSolutions() {
		return solutions;
	}
	public void setSolutions(double[] solutions) {
		this.solutions = solutions;
	}
	public double getSolutionI() {
		return solutionI;
	}
	public void setSolutionI(double solutionI) {
		this.solutionI = solutionI;
	}
	public double getSolutionII() {
		return solutionII;
	}
	public void setSolutionII(double solutionII) {
		this.solutionII = solutionII;
	}

}
