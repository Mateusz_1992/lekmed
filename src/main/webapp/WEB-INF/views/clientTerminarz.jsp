<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
 	</head>
	<body>
	 	<div id="legend">
	      <legend class=""><spring:message code="clientTerminarz.legenda"/></legend>
	    </div>
		<c:if  test="${!empty list}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="clientTerminarz.lp"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientTerminarz.lekarz"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientTerminarz.nrRoom"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientTerminarz.specjalizacja"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientTerminarz.godziny"/></span></th>
			                                </tr>
			                            </thead>
			                            <tbody>
				                            <c:forEach items="${list}" var="user" varStatus="index">
				                                <c:forEach items="${user.getUserRole()}" var="role">				
													<c:if test="${role.roleName == 'ROLE_USER'}">				
														<tr>
						                                	<td class="text-center" style="width: 4%;">
						                                		<a>${index.index + 1}</a>
						                                	</td>
						                                    <td class="text-center" style="width: 10%;">
						                                        <a class="user-link">${user.name}</a>
						                                        <a class="user-link">${user.surname}</a>
						                                    </td>
						                                    <td class="text-center" style="width: 10%;">
						                                        <a class="user-link">${user.room.roomNumber}</a>
						                                    </td>
						                                    <td class="text-center" style="width: 10%;">
						                                        <a class="user-link">${user.room.specialization}</a>
						                                    </td>
						                                    <td class="text-center">
						                                    	<c:if test="${! empty user.getSchedule()}">
							                                    	<c:forEach items="${user.getSchedule()}" var="schedule">
								                                    		| <a class="user-link">${schedule.date} : ${schedule.startHour} - ${schedule.endHour}</a> |
							                                    	</c:forEach>
						                                    	</c:if>
						                                    	<c:if test="${empty user.getSchedule()}">
						                                    		<td>
							                                    			<a class="user-link"><spring:message code="clientTerminarz.brak"/></a>
							                                    	</td>
							                                    	<td>
							                                    			<a>-</a>
							                                    	</td>
						                                    	</c:if>
						                                    </td>
					    								</tr>				
													</c:if>								
												</c:forEach>
			                        		</c:forEach>
			                      		</tbody>
			                 		</table>
			            		</div>
			           		</div>
			      		</div>
			  		</div>
				</div>
			</div>
		</c:if>
	</body>
</html>