<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	</head>
	<body>
	 	<div id="legend">
	      <legend class="">Edycja Terminarza</legend>
	    </div>
    	<div>
		<label>Dodaj schedule </label>
			<button type="button" class="class="btn btn-primary btn-sm"" onclick="window.location.href='userDodajSchedule.html'">
	    		<span class="glyphicon glyphicon-plus"></span>
	    	</button>
		</div>
    	<hr>
		<c:if  test="${!empty listSchedule}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="userEdytujTerminarz.lp"/></span></th>
			                                <th class="text-center"><span><spring:message code="userEdytujTerminarz.data"/></span></th>
			                                <th class="text-center"><span><spring:message code="userEdytujTerminarz.rozpoczecie"/></span></th>
			                                <th class="text-center"><span><spring:message code="userEdytujTerminarz.zakonczenie"/></span></th>
			                                <th>&nbsp;</th>
			                                </tr>
			                            </thead>
			                            <tbody>
				                            <c:forEach items="${listSchedule}" var="schedule" varStatus="index">
				                                <tr>
				                                	<td class="text-center">
				                                		<a>${index.index + 1}</a>
				                                	</td>
				                                    <td class="text-center">
				                                        <a class="user-link">${schedule.date}</a>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${schedule.startHour}</a>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${schedule.endHour}</a>
				                                    </td>
				                                    <td  class="text-center">
				                                        <button type="button" class="class="btn btn-primary btn-xs"" onclick="window.location.href='userDodajSchedule.html?scheduleId=${schedule.id}'">
				                                        	<span class="glyphicon glyphicon-pencil"></span>
				                                        </button>
				                                        <button type="button" class="class="btn btn-danger btn-xs"" onclick="window.location.href='userDeleteSchedule/${schedule.id}.html'">
				                                        	<span class="glyphicon glyphicon-trash"></span>
				                                        </button>
				                                    </td>
				                                </tr>
				                        	</c:forEach>
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</c:if>
		<c:if  test="${empty listSchedule}">
			<div class="text-center" id="legend">
	      		<span><spring:message code="userEdytujTerminarz.empty"/></span>
			</div>
		</c:if>
	</body>
</html>