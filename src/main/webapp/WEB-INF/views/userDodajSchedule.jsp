<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	</head>
	<body>
		<div id="legend">
			<legend class=""><spring:message code="userDodajSchedule.legenda"/></legend>
		</div>
		<form:form class="form-horizontal" action='addSchedule.html' method="POST" modelAttribute="edit">
			<fieldset>
				<div class="control-group">
			    	<form:hidden path="id"/>
			    </div>
			    <div class="control-group">
			      <label class="control-label"  for="date"><spring:message code="userDodajSchedule.data"/></label>
			      <div class="controls">
			        <form:input type="text" id="date" path="date" name="date" placeholder="" class="input-xlarge"/>
			      </div>
			    </div>
			 
			    <div class="control-group">
			      <!-- E-mail -->
			      <label class="control-label" for="startHour"><spring:message code="userDodajSchedule.startH"/></label>
			      <div class="controls">
			        <form:input type="text" id="startHour" path="startHour" name="startHour" placeholder="08:00" class="input-xlarge"/>
			      </div>
			    </div>
			 
			    <div class="control-group">
			      <!-- Password-->
			      <label class="control-label" for="endHour"><spring:message code="userDodajSchedule.endH"/></label>
			      <div class="controls">
			        <form:input type="text" id="endHour" path="endHour" name="endHour" placeholder="" class="input-xlarge"/>
			      </div>
			    </div>
			    <br>
			    <label>
			    	<spring:message code="userDodajSchedule.wymagane"/>
			    </label>
			    <div class="control-group">
			      <c:if test="${edit.id > 0}">
				      <div class="controls">
				        <button class="btn btn-success"><spring:message code="userDodajSchedule.przyciskE"/></button>
				      </div>
			      </c:if>
			      <c:if test="${edit.id == 0}">
				      <div class="controls">
				        <button class="btn btn-success"><spring:message code="userDodajSchedule.przyciskD"/></button>
				      </div>
			      </c:if>
			    </div>
			</fieldset>
		</form:form>
	</body>
</html>