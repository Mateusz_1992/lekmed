<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">      
	</head>
  	<body>
		<form:form class="form-horizontal" action='adminAddUser.html' method="POST" modelAttribute="user">
			<fieldset>
			  	<c:if test="${user.id > 0}">
				    <div id="legend">
				      <legend class=""><spring:message code="adminDodajUzytkownika.legenda1"/></legend>
				    </div>
			    </c:if>
			    <c:if test="${address.id == 0}">
			    	<div id="legend">
				      <legend class=""><spring:message code="adminDodajUzytkownika.legenda2"/></legend>
				    </div>
			    </c:if>
			    <div class="control-group">
			     <form:hidden path="id"/>
			    </div>
			    <div class="control-group">
			      	<label class="control-label"  for="login"><spring:message code="adminDodajUzytkownika.login"/></label>
			      	<spring:bind path="login">
				      	<div class="controls ${status.error ? 'has-error' : ''}">
				        	<form:input type="text" id="login" path="login" name="login" placeholder="" class="input-xlarge"/>
				        	<form:errors path="login"></form:errors>
				      	</div>
			      	</spring:bind>
			    </div>			 
			 	<div class="control-group">
			      	<label class="control-label"  for="haslo"><spring:message code="adminDodajUzytkownika.password"/></label>
			      	<div>
			        	<a id="haslo" href="adminEdytujHaslo/${user.id}.html"><spring:message code="adminDodajUzytkownika.password.label"/></a>
			      	</div>
			    </div>			 			 
			    <div class="control-group">
			      	<label class="control-label" for="name"><spring:message code="adminDodajUzytkownika.imie"/></label>
			      	<spring:bind path="name">
			      		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="name" path="name" name="name" placeholder="" class="input-xlarge"/>
			        		<form:errors path="name"></form:errors>
			      		</div>
			      	</spring:bind>
			    </div>			 
			    <div class="control-group">
			      	<label class="control-label"  for="surname"><spring:message code="adminDodajUzytkownika.nazwisko"/></label>
			      	<spring:bind path="surname">
			      		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="surname" path="surname"  name="surname" placeholder="" class="input-xlarge"/>
			        		<form:errors path="surname"></form:errors>
			      		</div>
			      	</spring:bind>
			    </div>			  			    
			    <div class="control-group">
			      	<label class="control-label"  for="email"><spring:message code="adminDodajUzytkownika.email"/></label>
			      	<spring:bind path="email">
			      		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="email" path="email"  name="email" placeholder="" class="input-xlarge"/>
			        		<p class="help-block"><spring:message code="adminDodajUzytkownika.email.help"/></p>
			        		<form:errors path="email"></form:errors>
			      		</div>
			      	</spring:bind>
			    </div>
			    <div class="control-group">			    
			      	<label class="control-label"  for="pesel"><spring:message code="adminDodajUzytkownika.pesel"/></label>
			      	<spring:bind path="pesel">
			      		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="pesel" path="pesel"  name="pesel" placeholder="" class="input-xlarge"/>
			        		<p class="help-block"><spring:message code="adminDodajUzytkownika.pesel.help"/></p>
			       	 		<form:errors path="pesel"></form:errors>
			      		</div>
			      	</spring:bind>
			    </div>			    
			    <div class="control-group">
			      	<label class="control-label"  for="phoneNumber"><spring:message code="adminDodajUzytkownika.nrtelefonu"/></label>
			       	<spring:bind path="phoneNumber">
			      		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="phoneNumber" path="phoneNumber"  name="phoneNumber" placeholder="" class="input-xlarge"/>
			      			<p class="help-block"><spring:message code="adminDodajUzytkownika.nrtelefonu.help"/></p>
			        		<form:errors path="phoneNumber"></form:errors>
			      		</div>
			      	</spring:bind>
			    </div>			  			    
			    <div class="control-group">			      
			      	<label class="control-label"  for="address"><spring:message code="adminDodajUzytkownika.adres"/></label>
			      	<div class="controls">			      
			       		<form:select path="address" id="address" name="address">
							<c:forEach items="${addressesList}" var="address">
        						<option value="${address.id}" ${address.id == selectedAddress ? 'selected="selected"' : ''}>${address.city}&nbsp;${address.street}&nbsp;${address.homeNumber}&nbsp;${address.flatNumber}&nbsp;${address.postCode}</option>
   							</c:forEach>
						</form:select>			        
				  	</div>
			    </div>			    			    
			    <div class="control-group">			      
			      	<label class="control-label"  for="room"><spring:message code="adminDodajUzytkownika.room"/></label>
			      	<div class="controls">
			       		<form:select path="room" id="room" name="room">
			       			<option value="0">&nbsp;</option>
							<c:forEach items="${roomList}" var="room">
	        					<option value="${room.id}" ${room.id == selectedRoom ? 'selected="selected"' : ''}>${room.roomNumber}&nbsp;${room.specialization}</option>
	   						</c:forEach>
						</form:select>		        
				  	</div>
			    </div>
			    <div class="control-group">
			      	<label class="control-label"  for="dataUrodzenia"><spring:message code="adminDodajUzytkownika.dataurodzenia"/></label>
			      	<spring:bind path="dataUrodzenia">
			      		<div class="controls ${status.error ? 'has-error' : ''}">
			         		<form:input type="text" id="dataUrodzenia" path="dataUrodzenia" class="controls" placeholder=" "></form:input>
	           	  			<p class="help-block"><spring:message code="adminDodajUzytkownika.dataurodzenia.help"/></p>
			        		<form:errors path="dataUrodzenia"></form:errors>
	           	  		</div>
	           	  	</spring:bind>
			    </div>
				<div class="control-group">
					<c:if test="${user.id > 0 }">
				      	<div class="controls">
				        	<button class="btn btn-success"><spring:message code="adminDodajUzytkownika.przyciskE"/></button>
				      	</div>
				    </c:if>
			    	<c:if test="${user.id <= 0 }">
			      		<div class="controls">
			        		<button class="btn btn-success"><spring:message code="adminDodajUzytkownika.przyciskD"/></button>
			      		</div>
			    	</c:if>
				</div>
			</fieldset>
		</form:form>
	</body>
</html>