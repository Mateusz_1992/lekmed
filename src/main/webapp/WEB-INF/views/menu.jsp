<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	<!-- Bootstrap core CSS -->
	<link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	</head>
	<body>
		<div class="profile-sidebar">
			<c:if test="${pageContext.request.userPrincipal.name != null}">
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<h3><c:out value="${pageContext.request.userPrincipal.name}"></c:out></h3>
					</div>
					<hr>
					<div class="profile-usertitle-job">
						<security:authorize ifAllGranted="ROLE_ADMIN">
							<spring:message code="menu.rola.admin"/> 
						</security:authorize>
						<security:authorize ifAllGranted="ROLE_USER">
							<spring:message code="menu.rola.user"/> 
						</security:authorize>
						<security:authorize ifAllGranted="ROLE_CLIENT">
							<spring:message code="menu.rola.client"/>  
						</security:authorize>
					</div>
				</div>
			</c:if>
			<hr>
			<!-- MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="active">
						<h1>
							<i class=" glyphicon glyphicon-globe"></i>
							<spring:message code="menu.menu"/>
						</h1>
						<hr>
					</li>
					<security:authorize ifAllGranted="ROLE_ADMIN">
						<li class="active">
							<h2>
								<i class="glyphicon glyphicon-home"></i>
								<spring:message code="menu.rola.admin"/>  
							</h2>
						</li>
						<li>
							<a href="adminListaUzytkownikow.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.panelAdmin.listaUzytkownikow"/>
							</a>
						</li>
						<li>
							<a href="edytujKonto.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.edytujKonto"/>
							</a>
						</li>
						<li>
							<a href="edytujAdres.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.edytujAdres"/>
							</a>
						</li>
						<li>
							<a href="adminListaWizyt.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelAdmin.listaWizyt"/>
							</a>
						</li>
						<li>
							<a href="adminListaAdresow.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelAdmin.listaAdresow"/>
							</a>
						</li>
						<hr>
					</security:authorize>
					<security:authorize ifAllGranted="ROLE_USER">
						<li class="active">
							<h2>
								<i class="glyphicon glyphicon-home"></i>
								<spring:message code="menu.rola.user"/>
							</h2>
						</li>
						<li>
							<a href="userEdytujTerminarz.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelLekarz.edytujTerminarz"/>
							</a>
						</li>
						<li>
							<a href="edytujKonto.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.edytujKonto"/>
							</a>
						</li>
						<li>
							<a href="edytujAdres.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.edytujAdres"/>
							</a>
						</li>
						<li>
							<a href="userListaWizyt.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelLekarz.listaWizyt"/>
							</a>
						</li>
						<li>
							<a href="userRachunki.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelLekarz.rachunki"/>
							</a>
						</li>
						<hr>
					</security:authorize>
					<security:authorize ifAllGranted="ROLE_CLIENT">
						<li class="active">
							<h2>
								<i class="glyphicon glyphicon-home"></i>
								<spring:message code="menu.rola.client"/>
							</h2>
						</li>
						<li>
							<a href="clientRachunki.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelPacjent.rachunki"/>
							</a>
						</li>
						<li>
							<a href="edytujKonto.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.edytujKonto"/>
							</a>
						</li>
						<li>
							<a href="edytujAdres.html">
								<i class="glyphicon glyphicon-user"></i>
								<spring:message code="menu.edytujAdres"/>
							</a>
						</li>
						<li>
							<a href="clientMojeWizyty.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelPacjent.listaMojeWizyty"/>
							</a>
						</li>
						<li>
							<a href="clientTerminarz.html">
								<i class="glyphicon glyphicon-tasks"></i>
								<spring:message code="menu.panelPacjent.godzinyPrzyjec"/>
							</a>
						</li>
						<li>
							<a href="clientDodajWizyte.html">
								<i class="glyphicon glyphicon-flag"></i>
								<spring:message code="menu.panelPacjent.zapiszNaWizyte"/>
							</a>
						</li>
						<hr>
					</security:authorize>
					<li>
						<a href="quadraticEquation.html">
							<i class="glyphicon glyphicon-flag"></i>
							<spring:message code="menu.rownanieKwadratowe"/>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</body>
</html>