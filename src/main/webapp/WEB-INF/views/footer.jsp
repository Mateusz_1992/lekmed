<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content=""> 
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value='/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css'/>" rel="stylesheet">
        
</head>
  <body>
      <footer style="margin: 15px;">
      	<p class="pull-right"><a href="#"><spring:message code="footer.powrotGora"/></a></p>
      	<p><spring:message code="footer.podpis"/></p>
      </footer>
	</body>
</html>
 
