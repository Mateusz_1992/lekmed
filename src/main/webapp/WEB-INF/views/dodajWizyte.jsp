<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
		
		
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	</head>
	<body>
		<div id="legend">
      		<legend><spring:message code="dodajWizyte.legend"/></legend>
    	</div>
		<form:form class="form-horizontal" action="nowaWizyta.html" method="POST" modelAttribute="newAppointment">
  			<fieldset>
				<form:hidden path="id"/>
  				<form:hidden path="patient"/>
				<div class="control-group">
		  			<label class="control-label" for="date"><spring:message code="dodajWizyte.date"/></label>
			      	<spring:bind path="date">
			            <div class="controls ${status.error ? 'has-error' : ''}">
			                <form:input path="date" id="datepicker"/>
			                <p class="help-block"><spring:message code="dodajWizyte.date.help"/></p>
			                <form:errors path="date"></form:errors>
			            </div>
					</spring:bind>
			     </div>
		 		 <div class="control-group">
		  			<label class="control-label" for="hour"><spring:message code="dodajWizyte.hour"/></label>
			      	<spring:bind path="hour">
			            <div class="controls ${status.error ? 'has-error' : ''}">
			                <form:input type="text" id="hour" path="hour" class="controls" placeholder=" "></form:input>
			                <p class="help-block"><spring:message code="dodajWizyte.hour.help"/></p>
			                <form:errors path="hour"></form:errors>
			            </div>
					</spring:bind>
			    </div>
				<div class="control-group">
			    	<label class="control-label"  for="user"><spring:message code="dodajWizyte.lekarz"/></label>
			      	<div class="controls">
			    		<form:select path="user" id="user" name="user">
							<c:forEach items="${listaLekarzy}" var="user">
        						<option value="${user.id}">${user.name}&nbsp;${user.surname}&nbsp;${user.room.specialization}</option>
   							</c:forEach>
						</form:select>
				  	</div>
				</div>
 				<br>
			    <div class="control-group">
			      <!-- Button -->
			      <div class="controls">
			        <button class="btn btn-success" type="submit"><spring:message code="dodajWizyte.dodajWiz"/></button>
			      </div>
			    </div>
			</fieldset>
		</form:form>
	</body>
</html>