<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
	<head>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  	<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
	</head>
	<body onload='document.loginForm.username.focus();'>
		<div class="container">
    		<div class="row">
        		<div class="col-md-offset-5 col-md-3">
	        		<c:if test="${not empty error}">						
						<div class="alert alert-danger">
  							<strong><spring:message code="login.blad"/></strong>
						</div>
					</c:if>
					<c:if test="${not empty msg}">
						<div class="alert alert-success">
  							<strong><spring:message code="login.wylogowanie"/></strong>
						</div>
					</c:if>
					<form class="form-signin" name='loginForm' action="<c:url value='j_spring_security_check' />" method='POST'>
	               		<input type="text"  name='login' id="inputEmail"  class="form-control" placeholder="Login" required autofocus>
	               		<input type="password" name='password' id="inputPassword"  class="form-control" placeholder="Password" required>
	               		<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit"><spring:message code="login.przyciskZaloguj"/></button>
	               		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	           		</form>
        		</div>
    		</div>
		</div>
	</body>
</html>