<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">

</head>
<body>
	<div class="container">
	  <div class="row">
	    <div class="span12">
	      <div class="hero-unit center">
	          <div class="text-center">
                 <h2 class="without-margin"><spring:message code="error403.header1"/><span class="text-warning"><big>403</big></span></h2>
                 <h4 class="text-warning"><spring:message code="error403.header1"/></h4>
			   </div>
	          <a href="/lekmed" class="btn btn-large btn-info"><i class="icon-home icon-white"></i><spring:message code="error403.przycisk"/></a>
	      </div>
	    </div>
	 </div>
	</div>
</body>
</html>