<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	  
 	</head>
	<body>
	 	<div id="legend">
	      <legend class=""><spring:message code="clientRachunki.legenda"/></legend>
	    </div>
		<c:if  test="${!empty receipList}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="clientRachunki.lp"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientRachunki.cena"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientRachunki.wizyta"/></span></th>
			                                <th class="text-center"><span><spring:message code="clientRachunki.lekarz"/></span></th>
			                                <th>&nbsp;</th>
			                                </tr>
			                            </thead>
			                            <tbody>
				                            <c:forEach items="${receipList}" var="receipt" varStatus="index">
				                                <tr>
				                                	<td class="text-center">
				                                		<a>${index.index + 1}</a>
				                                	</td>
				                                    <td class="text-center">
				                                        <a class="user-link">${receipt.cost} PLN</a>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${receipt.appointment.date} ${receipt.appointment.hour} ${receipt.appointment.user.room.roomNumber} ${receipt.appointment.user.room.specialization}</a>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${receipt.appointment.user.name} ${receipt.appointment.user.surname}</a>
				                                    </td>
				                                    <td  class="text-center">
				                                        <button type="button" class="btn btn-danger btn-md" onclick="window.location.href='/lekmed/generatePdf/${receipt.id}.html'">
				                                        <span class="glyphicon glyphicon-save-file"></span>
				                                        </button>
				                                    </td>
				                                </tr>
				                           	</c:forEach>
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</c:if> 
	</body>
</html>