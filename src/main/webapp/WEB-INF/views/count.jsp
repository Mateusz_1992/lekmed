<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	    
	    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    </head>
	<body>
		<div id="legend">
		      <legend class=""><spring:message code="count.legenda"/></legend>
		</div>
		<c:if  test="${!empty equations}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="count.a"/></span></th>
			                                <th class="text-center"><span><spring:message code="count.b"/></span></th>
			                                <th class="text-center"><span><spring:message code="count.c"/></span></th>
			                                <th class="text-center"><span><spring:message code="count.delta"/></span></th>
			                                <th class="text-center"><span><spring:message code="count.x1"/></span></th>
			                                <th class="text-center"><span><spring:message code="count.x2"/></span></th>
			                                <th>&nbsp;</th>
			                                </tr>
			                            </thead>
			                            <tbody>
			                            <c:forEach items="${equations}" var="equation" varStatus="index">
			                                <tr>
			                                    <td class="text-center">
			                                        <a class="user-link">${equation.coefficientA}</a>
			                                    </td>
			                                    <td class="text-center">
			                                        <a class="user-link">${equation.coefficientB}</a>
			                                    </td>
			                                    <td class="text-center">
			                                        <a class="user-link">${equation.coefficientC}</a>
			                                    </td>
			                                    <td class="text-center">
			                                    	<a class="user-link">${equation.delta}</a>
			                                    </td>
			                                    <c:if test="${equation.delta > 0}">
				                                    <td class="text-center">
				                                    	<a class="user-link">${equation.solutionI}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link">${equation.solutionII}</a>
				                                    </td>
			                                    </c:if>
			                                    <c:if test="${equation.delta == 0}">
				                                    <td class="text-center">
				                                    	<a class="user-link">${equation.solutionI}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link">-</a>
				                                    </td>
			                                    </c:if>
			                                    <c:if test="${equation.delta < 0}">
				                                    <td class="text-center">
				                                    	<a class="user-link"><spring:message code="count.deltaujemna"/></a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link"><spring:message code="count.deltaujemna"/></a>
				                                    </td>
			                                    </c:if>
			                                </tr>
			                                </c:forEach>
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</c:if>
		<c:if  test="${empty equations}">
			<div class="text-center" id="legend">
	      		<legend class=""><spring:message code="count.zle"/></legend>
			</div>
		</c:if>
	</body>
</html>