<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	        
	    <script src='https://www.google.com/recaptcha/api.js'></script>    
 	</head>
  	<body>
  		<div id="legend">
      		<legend><spring:message code="rejestracja.legenda"/></legend>
    	</div>
  		<form:form class="form-horizontal" action="addUser.html" method="POST" modelAttribute="userForm">
  			<fieldset>
				<div class="left">
  					<form:hidden path="id"/>
			  		<div class="control-group">
			  			<label class="control-label" for="login"><spring:message code="rejestracja.login"/></label>
				      	<spring:bind path="login">
				            <div class="controls ${status.error ? 'has-error' : ''}">
				                <form:input type="text" id="login" path="login" class="controls" placeholder="Login"></form:input>
				                <p class="help-block"><spring:message code="rejestracja.login.help"/></p>
				                <form:errors path="login"></form:errors>
				            </div>
						</spring:bind>
				     </div>
			 		 <div class="control-group">
			  			<label class="control-label" for="password"><spring:message code="rejestracja.haslo"/></label>
				      	<spring:bind path="login">
				            <div class="controls ${status.error ? 'has-error' : ''}">
				                <form:input type="password" id="password" path="password" class="controls" placeholder="Password"></form:input>
				                <p class="help-block"><spring:message code="rejestracja.haslo.help"/></p>
				            	<form:errors path="password"></form:errors>
				        	</div>
						</spring:bind>
				    </div>
				   	<div class="control-group">
				  		<label class="control-label" for="passwordConfirm"><spring:message code="rejestracja.hasloPowtorz"/></label>
					    <spring:bind path="passwordConfirm">
					    	<div class="controls ${status.error ? 'has-error' : ''}">
					        	<form:input type="password" id="passwordConfirm" path="passwordConfirm" class="controls" placeholder="Password Confirm"></form:input>
					            <p class="help-block"><spring:message code="rejestracja.hasloPowtorz.help"/></p>
					            <form:errors path="passwordConfirm"></form:errors>
					        </div>
						</spring:bind>
					</div>
			 		<div class="control-group">
			  			<label class="control-label" for="email"><spring:message code="rejestracja.email"/></label>
				      	<spring:bind path="email">
				            <div class="controls ${status.error ? 'has-error' : ''}">
				                <form:input type="email" id="email" path="email" class="controls" placeholder="name@example.pl"></form:input>
				                <p class="help-block"><spring:message code="rejestracja.email.help"/></p>
				                <form:errors path="email"></form:errors>
				            </div>
						</spring:bind>
				    </div>
 				</div>
   				<div class="center" style="position: absolute;top: 10%;left: 33%;">
		   			<div class="control-group">
		  				<label class="control-label" for="name"><spring:message code="rejestracja.imie"/></label>
			      		<spring:bind path="name">
			            	<div class="controls ${status.error ? 'has-error' : ''}">
			                	<form:input type="text" id="name" path="name" class="controls" placeholder="Imie"></form:input>
			                	<form:errors path="name"></form:errors>
			            	</div>
						</spring:bind>
			     	</div>
			 		<div class="control-group">
			  			<label class="control-label" for="surname"><spring:message code="rejestracja.nazwisko"/></label>
				      	<spring:bind path="surname">
				            <div class="controls ${status.error ? 'has-error' : ''}">
				                <form:input type="text" id="surname" path="surname" class="controls" placeholder="Nazwisko"></form:input>
				                <form:errors path="surname"></form:errors>
				            </div>
						</spring:bind>
				    </div>
				   	<div class="control-group">
				  		<label class="control-label" for="pesel"><spring:message code="rejestracja.pesel"/></label>
					    <spring:bind path="pesel">
					    	<div class="controls ${status.error ? 'has-error' : ''}">
					        	<form:input type="text" id="pesel" path="pesel" class="controls" placeholder="PESEL"></form:input>
					            <p class="help-block"><spring:message code="rejestracja.pesel.help"/></p>
					            <form:errors path="pesel"></form:errors>
					        </div>
						</spring:bind>
					</div>
			   		<div class="control-group">
			  			<label class="control-label" for="phoneNumber"><spring:message code="rejestracja.nrtelefonu"/></label>
				      	<spring:bind path="phoneNumber">
				            <div class="controls ${status.error ? 'has-error' : ''}">
				                <form:input type="text" id="phoneNumber" path="phoneNumber" class="controls" placeholder="Numer telefonu"></form:input>
				                <p class="help-block"><spring:message code="rejestracja.nrtelefonu.help"/></p>
				                <form:errors path="phoneNumber"></form:errors>
				            </div>
						</spring:bind>
				    </div>
				 	<div class="control-group">
		  				<label class="control-label" for="dataUrodzenia"><spring:message code="rejestracja.dataurodzenia"/></label>
			      		<spring:bind path="dataUrodzenia">
			            	<div class="controls ${status.error ? 'has-error' : ''}">
			                	<form:input type="text" id="dataUrodzenia" path="dataUrodzenia" class="controls" placeholder="YYYY-MM-DD"></form:input>
			                	<p class="help-block"><spring:message code="rejestracja.dataurodzenia.help"/></p>
			                	<form:errors path="dataUrodzenia"></form:errors>
			            	</div>
						</spring:bind>
			    	</div>
				</div>
			 	<br>
			 	<br>
			 	<label><font color="red"><spring:message code="rejestracja.wymagane.opis"/></font></label>
			 	<br>
			 	<br>
			 	<br>
			 	<div id="captcha_paragraph">
					<c:if test="${invalidRecaptcha == true}">
						<span class="error_form_validation"><font face="Tahoma" color="red"><spring:message code="rejestracja.invalid.captcha"/></font></span>
					</c:if>
					<% 
						ReCaptcha c = ReCaptchaFactory.newReCaptcha("6LdA1RAUAAAAAC7-yMnPMzZlACwa2qlv2wYQR4sA", "6LdA1RAUAAAAAN_e_zWv-ec__rIVWcFxcZt_9kTa", false);
					   	out.print(c.createRecaptchaHtml(null, null));
					%>
				</div>
			 	<br>
			 	<br>	
			    <div class="control-group">
			      <!-- Button -->
			      <div class="controls">
			        <button class="btn btn-success" type="submit"><spring:message code="rejestracja.przycisk"/></button>
			      </div>
			    </div>
  			</fieldset>
		</form:form>
  	</body>
</html>




