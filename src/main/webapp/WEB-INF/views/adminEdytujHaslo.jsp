<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	</head>
  	<body>
		<div id="legend">
			<legend class=""><spring:message code="adminEdytujHaslo.legenda"/></legend>
		</div>
		<form:form class="form-horizontal" action='/lekmed/adminEditPassword.html' method="POST" modelAttribute="edit">
			<fieldset>    
			    <div class="control-group">
			     	<form:hidden path="id"/>
			    </div>			    			    				
			    <div class="control-group">
			      	<label class="control-label"  for="login"><spring:message code="adminEdytujHaslo.login"/></label>
			    	<spring:bind path="login">
				      	<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="login" path="login" name="login" placeholder="" class="input-xlarge"/>
				        	<form:errors path="login"></form:errors>
				      	</div>
			      	</spring:bind>
			    </div>
			 	<div class="control-group">
			      	<label class="control-label"  for="password"><spring:message code="adminEdytujHaslo.password"/></label>
			    	<spring:bind path="password">
				      	<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="password" id="password" path="password" name="password" placeholder="" class="input-xlarge"/>
			      			<p class="help-block"><spring:message code="adminEdytujHaslo.password.help"/></p>
				        		<form:errors path="password"></form:errors>
				      	</div>
			      	</spring:bind>
			    </div>			    
			    <div class="control-group">
			      	<label class="control-label"  for="passwordConfirm"><spring:message code="adminEdytujHaslo.confirmPassword"/></label>
			      	<spring:bind path="passwordConfirm">
				      	<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="password" id="passwordConfirm" path="passwordConfirm" name="passwordConfirm" placeholder="" class="input-xlarge"/>
				        	<form:errors path="passwordConfirm"></form:errors>
				      	</div>
			      	</spring:bind>
			    </div>
			    <br>
			    <label>
			    	<spring:message code="adminEdytujHaslo.wymagane"/>
			    </label>
			    <br>
			    <div class="control-group">
			      	<div class="controls">
			        	<button class="btn btn-success"><spring:message code="adminEdytujHaslo.przycisk"/></button>
			      	</div>
			    </div>   
			</fieldset>
		</form:form>
	</body>
</html>