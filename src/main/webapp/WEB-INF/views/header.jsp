<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content=""> 
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value='/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css'/>" rel="stylesheet">   
	    
	    <link href="<c:url value='/resources/css/langButton.css'/>" rel="stylesheet">   
	</head>
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
			    	<img src="<c:url value='resources/images/logo.gif'/>" class="img-circle" alt="Logo" width="120" height="100"/>
			    </div>
			    <ul class="nav navbar-nav">
			    	<li class="active"><a href="/lekmed"><spring:message code="header.stronaDomowa"/></a></li>
			      	<security:authorize ifAllGranted="ROLE_ADMIN">
				    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><spring:message code="header.panelAdmin"/><span class="caret"></span></a>
				        	<ul class="dropdown-menu">
				          		<li><a href="adminListaUzytkownikow.html"><spring:message code="header.panelAdmin.listaUzytkownikow"/></a></li>
				          		<li><a href="adminListaWizyt.html"><spring:message code="header.panelAdmin.listaWizyt"/></a></li>
				          		<li><a href="adminListaAdresow.html"><spring:message code="header.panelAdmin.listaAdresow"/></a></li>
				          		<li><a href="edytujKonto.html"><spring:message code="header.edytujKonto"/></a></li>
				          		<li><a href="edytujAdres.html"><spring:message code="header.edytujAdres"/></a></li>
				        	</ul>
				      	</li>
				  	</security:authorize>
				  	<security:authorize ifAllGranted="ROLE_USER">
				    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><spring:message code="header.panelLekarz"/><span class="caret"></span></a>
				        	<ul class="dropdown-menu">
				          		<li><a href="userEdytujTerminarz.html"><spring:message code="header.panelLekarz.edytujTerminarz"/></a></li>
				          		<li><a href="userListaWizyt.html"><spring:message code="header.panelLekarz.listaWizyt"/></a></li>
				          		<li><a href="userRachunki.html"><spring:message code="header.panelLekarz.rachunki"/></a></li>
				          		<li><a href="edytujKonto.html"><spring:message code="header.edytujKonto"/></a></li>
				          		<li><a href="edytujAdres.html"><spring:message code="header.edytujAdres"/></a></li>
				        	</ul>
				      	</li>
				  	</security:authorize>
				  	<security:authorize ifAllGranted="ROLE_CLIENT">
				      	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><spring:message code="header.panelPacjent"/><span class="caret"></span></a>
				        	<ul class="dropdown-menu">
				          		<li><a href="clientListaWizyt.html"><spring:message code="header.panelPacjent.listaWizyt"/></a></li>
				          		<li><a href="clientMojeWizyty"><spring:message code="header.panelPacjent.listaMojeWizyty"/></a></li>
				          		<li><a href="clientTerminarz"><spring:message code="header.panelPacjent.godzinyPrzyjec"/></a></li>
				          		<li><a href="edytujKonto.html"><spring:message code="header.edytujKonto"/></a></li>
				          		<li><a href="edytujAdres.html"><spring:message code="header.edytujAdres"/></a></li>
				          		<li><a href="clientRachunki.html"><spring:message code="header.panelPacjent.clientRachunki"/></a></li>
				          		<li><a href="clientDodajWizyte.html"><spring:message code="header.panelPacjent.zapiszNaWizyte"/></a></li>
				        	</ul>
				      	</li>
				  	</security:authorize>
			      	<li><a href="quadraticEquation.html"><spring:message code="header.rownanieKwadratowe"/></a></li>
			    </ul>
			    <p style="position: absolute; right: 65px; top: 65px;" class="pull-right"> 
			    	<span>
			    		<input id="pl" type="button" value="    " onclick="window.location.href='?lang=pl'"> 
			    		| 
			    		<input id="en" type="button" value="    " onclick="window.location.href='?lang=en'">
			    		|
			    		<input id="de" type="button" value="    " onclick="window.location.href='?lang=de'">
					</span>
			    </p>
			    <c:if test="${pageContext.request.userPrincipal.name == null}">
				    <ul class="nav navbar-nav navbar-right">
				      <li><a href="rejestracja.html"><span class="glyphicon glyphicon-user"></span><spring:message code="header.rejestracja"/></a></li>
				      <li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span><spring:message code="header.zaloguj"/></a></li>
				    </ul>
			    </c:if>
			    <c:if test="${pageContext.request.userPrincipal.name != null}">
				    <ul class="nav navbar-nav navbar-right">
				      <li><a href="javascript:formSubmit()"><span class="glyphicon glyphicon-log-out"></span><spring:message code="header.wyloguj"/></a></li>
				    </ul>
			    </c:if>
			</div>
		</nav>
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<!-- csrt for log out-->
		<form action="${logoutUrl}" method="post" id="logoutForm">
		  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() 
			{
				document.getElementById("logoutForm").submit();
			}
		</script>	
	</body>
</html>