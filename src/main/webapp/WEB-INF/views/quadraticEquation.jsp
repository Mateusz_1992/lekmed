<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
		
		
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">        
	</head>
  	<body>
  		<div id="legend">
			<legend><spring:message code="quadraticEquation.legenda"/></legend>
    	</div>
  		<form:form class="form-horizontal" action="count.html" method="POST" modelAttribute="equation">
  			<fieldset>
		 		<div class="control-group">
		  			<label class="control-label" for="coefficientAC"><spring:message code="quadraticEquation.coefficientA"/></label>
		        	<spring:bind path="coefficientA">
			            <div class="controls ${status.error ? 'has-error' : ''}">
			                <form:input type="text" id="coefficientAC" path="coefficientA" class="controls" placeholder="Wprowadz wspolczynnik a"></form:input>
			                <p class="help-block"><spring:message code="quadraticEquation.coefficientA"/></p>
			                <form:errors path="coefficientA"></form:errors>
			            </div>
		            </spring:bind>
			    </div>
		   		<div class="control-group">
		  			<label class="control-label" for="coefficientB"><spring:message code="quadraticEquation.coefficientB"/></label>
			            <spring:bind path="coefficientB">
			            <div class="controls ${status.error ? 'has-error' : ''}">
			                <form:input type="text" id="coefficientB" path="coefficientB" class="controls" placeholder="Wprowadz wspolczynnik b"></form:input>
			                <p class="help-block"><spring:message code="quadraticEquation.coefficientB"/></p>
			                <form:errors path="coefficientB"></form:errors>
			            </div>
						</spring:bind>
			    </div>
		 		<div class="control-group">
		  			<label class="control-label" for="coefficientC"><spring:message code="quadraticEquation.coefficientC"/></label>
			            <spring:bind path="coefficientC">
			            <div class="controls ${status.error ? 'has-error' : ''}">
			                <form:input type="text" id="coefficientC" path="coefficientC" class="controls" placeholder="Wprowadz wspolczynnik C"></form:input>
			                <p class="help-block"><spring:message code="quadraticEquation.coefficientC"/></p>
			                <form:errors path="coefficientC"></form:errors>
			            </div>
						</spring:bind>
		     	</div>
				<br>
	 			<br>
	 			<hr>
			    <div class="control-group">
			      <!-- Button -->
			      <div class="controls">
			        <button class="btn btn-success" type="submit"><spring:message code="quadraticEquation.coeffBtn"/></button>
			      </div>
			    </div>
  			</fieldset>
		</form:form>
  	</body>
</html>