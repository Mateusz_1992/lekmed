<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
 	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
	
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
		
		<!-- Bootstrap core CSS -->
		<link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">  
	</head>
	<body>
		<div id="legend">
	      <legend class=""><spring:message code="edytujAdres.legenda"/></legend>
	    </div>
		<c:if test="${success}">
			<div class="alert alert-success">
				<strong><spring:message code="edytujAdres.good"/></strong>
			</div>	
		</c:if>
		<form:form class="form-horizontal" action='editAddress.html' method="POST" modelAttribute="editAdd">
			<fieldset>
 				<div class="control-group">
  					<form:hidden path="id"/>
				</div>
				<div class="control-group">
					<label class="control-label"  for="street"><spring:message code="edytujAdres.street"/></label>
					<spring:bind path="street">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" id="street" path="street" name="street" placeholder="" class="input-xlarge"/>
							<form:errors path="street"></form:errors>
							</div>
					</spring:bind>
				</div>
				<div class="control-group">
					<label class="control-label" for="homeNumber"><spring:message code="edytujAdres.homeNumber"/></label>
				  	<spring:bind path="homeNumber">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" id="homeNumber" path="homeNumber" name="homeNumber" placeholder="" class="input-xlarge"/>
							<form:errors path="homeNumber"></form:errors>
						</div>
					</spring:bind>
				</div>
				<div class="control-group">
					<label class="control-label" for="flatNumber"><spring:message code="edytujAdres.flatNumber"/></label>
					<spring:bind path="street">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" id="flatNumber" path="flatNumber" name="flatNumber" placeholder="" class="input-xlarge"/>
							<form:errors path="flatNumber"></form:errors>
						</div>
					</spring:bind>
				</div>
				<div class="control-group">
					<label class="control-label"  for="postCode"><spring:message code="edytujAdres.postCode"/></label>
					<spring:bind path="postCode">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" id="postCode" path="postCode"  name="postCode" placeholder="" class="input-xlarge"/>
							<p class="help-block"><spring:message code="edytujAdres.postCode.help"/></p>
							<form:errors path="postCode"></form:errors>
						</div>
					</spring:bind>
				</div>
				<div class="control-group">
					<label class="control-label"  for="city"><spring:message code="edytujAdres.city"/></label>
					<spring:bind path="city">
						<div class="controls ${status.error ? 'has-error' : ''}">
							<form:input type="text" id="city" path="city"  name="city" placeholder="" class="input-xlarge"/>
							<form:errors path="city"></form:errors>
						</div>
					</spring:bind>
				</div>
				<br>
				<label>
					<spring:message code="edytujAdres.wymagane"/>
				</label>
				<br>
				<label>
					<spring:message code="edytujAdres.opcjonalne"/>
				</label>
				<br>
				<div class="control-group">
				    <div class="controls">
				      <button class="btn btn-success"><spring:message code="edytujAdres.przycisk"/></button>
				    </div>
				</div>
			</fieldset>
		</form:form>
	</body>
</html>