<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	    
	    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    </head>
	<body>
		<div id="legend">
		      <legend class=""><spring:message code="adminListaAdresow.legenda"/></legend>
		</div>
		<div>
			<label><spring:message code="adminListaAdresow.add"/></label>
			<button type="button" onclick="window.location.href='adminDodajAdres.html'">
		    	<span class="glyphicon glyphicon-plus"></span>
		    </button>
		</div>
		<c:if  test="${!empty addressesList}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="adminListaAdresow.lp"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaAdresow.street"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaAdresow.homeNumber"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaAdresow.flatNumber"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaAdresow.postCode"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaAdresow.city"/></span></th>
			                                <th>&nbsp;</th>
			                                </tr>
			                            </thead>
			                            <tbody>
				                            <c:forEach items="${addressesList}" var="address" varStatus="index">
				                                <tr>
				                                	<td class="text-center">
				                                		<a>${index.index + 1}</a>
				                                	</td>
				                                    <td class="text-center" style="width: 18%;">
				                                        <a class="user-link">${address.street}</a>
				                                    </td>
				                                    <td class="text-center" style="width: 18%;">
				                                        <a class="user-link">${address.homeNumber}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<c:if test="${ empty address.flatNumber}">
				                                        	-
				                                        </c:if>
				                                        <c:if test="${! empty address.flatNumber}">
				                                        		<a class="user-link">${address.flatNumber}</a>
				                                        </c:if>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${address.postCode}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link">${address.city}</a>
				                                    </td>
				                                    <td  class="text-center">
				                                        <button type="button" class="class="btn btn-primary btn-xs"" onclick="window.location.href='adminDodajAdres.html?addressId=${address.id}'">
				                                        <span class="glyphicon glyphicon-pencil"></span>
				                                        </button>
				                                        <button type="button" class="class="btn btn-danger btn-xs"" onclick="window.location.href='adminDeleteAddress/${address.id}.html'">
				                                        <span class="glyphicon glyphicon-trash"></span>
				                                        </button>
				                                    </td>
				                                </tr>
			                                </c:forEach>
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</c:if>
		<c:if  test="${empty addressesList}">
				<div class="text-center" id="legend">
		      		<h2><spring:message code="adminListaAdresow.empty"/></h2>
				</div>
		</c:if>
	</body>
</html>