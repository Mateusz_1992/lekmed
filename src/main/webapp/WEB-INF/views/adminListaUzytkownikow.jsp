<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<!DOCTYPE html>
<html>
  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	    
	    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    </head>
	<body>
		<div id="legend">
		      <legend class=""><spring:message code="adminListaUzytkownikow.legenda"/></legend>
		</div>
		<div>
			<label><spring:message code="adminListaUzytkownikow.add"/></label>
			<button type="button" onclick="window.location.href='adminDodajUzytkownika.html'">
		    	<span class="glyphicon glyphicon-plus"></span>
		    </button>
		</div>
		<c:if test="${!empty userList}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.lp"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.uzytkownik"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.status"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.role"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.specjalizacja"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.email"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.dataUrodzenia"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.nrTelefonu"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.pesel"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.adres"/></span></th>
			                                <th class="text-center"><span><spring:message code="adminListaUzytkownikow.uprawnienia"/></span></th>
			                                <th>&nbsp;</th>
			                                </tr>
			                            </thead>
			                            <tbody>
			                            <c:forEach items="${userList}" var="user" varStatus="index">
			                                <tr>
			                                	<td class="text-center">
			                                		<a>${index.index + 1}</a>
			                                	</td>
			                                    <td class="text-center" style="width: 18%;">
			                                        <!--<img src="http://bootdey.com/img/Content/user_1.jpg" alt="">  -->
			                                        <a class="user-link">${user.name}</a>
			                                        <a class="user-link">${user.surname}</a>
			                                        
			                                    </td>
			                                    <td class="text-center">
			                                    	<c:if test="${user.isActive()}">
			                                        	<span class="label label-label label-success"><spring:message code="adminListaUzytkownikow.aktywny"/></span>
			                                        </c:if>
			                                        <c:if test="${!user.isActive()}">
			                                        	<span class="label label-label label-danger"><spring:message code="adminListaUzytkownikow.nieAktywny"/></span>
			                                        </c:if>
			                                    </td>
			                                    <td class="text-center">
			                                    	<span>	
														[
															<c:forEach items="${user.getUserRole()}" var="role">
																<c:if test="${role.roleName == 'ROLE_ADMIN'}">
																<spring:message code="adminListaUzytkownikow.admin"/>
																</c:if> 
																<c:if test="${role.roleName == 'ROLE_USER'}">
																<spring:message code="adminListaUzytkownikow.user"/>
																</c:if>
																<c:if test="${role.roleName == 'ROLE_CLIENT'}">
																<spring:message code="adminListaUzytkownikow.client"/>
																</c:if>
															</c:forEach>
													]
													</span>
			                                    </td>
			                                    <td class="text-center">
			                                    	<c:if test="${user.room.roomNumber == 0}">
			                                        	<a> - </a>
			                                        </c:if>
			                                        <c:if test="${user.room.roomNumber != 0}">
			                                        	<a class="user-link">${user.room.specialization}</a>
			                                        </c:if>
			                                    </td>
			                                    <td class="text-center">
			                                        <a class="user-link">${user.email}</a>
			                                    </td>
			                                    <td class="text-center">
			                                    	<a class="user-link">${user.dataUrodzenia}</a>
			                                    </td>
			                                    <td class="text-center">
			                                    	<a class="user-link">${user.phoneNumber}</a>
			                                    </td>
			                                    <td class="text-center">
			                                    	<a class="user-link">${user.pesel}</a>
			                                    </td>
			                                    <td class="text-center">
			                                    	<a class="user-link">${user.address.street} ${user.address.homeNumber} <c:if test="${! empty user.address.flatNumber}">/ ${user.address.flatNumber}</c:if> ${user.address.postCode} ${user.address.city}</a>
			                                    </td>
			                                    <td class="text-center">
			                                    	<button type="button" class="btn btn-primary btn-xs" onclick="window.location.href='/lekmed/adminRoleAdmin/${user.id}.html'">
						                                  ADMIN
						                            </button>
						                            <button type="button" class="btn btn-primary btn-xs" onclick="window.location.href='/lekmed/adminRoleClient/${user.id}.html'">
						                                  CLIENT
						                            </button>
						                            <button type="button" class="btn btn-primary btn-xs" onclick="window.location.href='/lekmed/adminRoleUser/${user.id}.html'">
						                                  USER
						                            </button>
			                                    </td>
			                                    <td  class="text-center">
			                                        <button type="button" onclick="window.location.href='adminActiveUzytkownik/${user.id}.html'">
			                                        <span class="glyphicon glyphicon-off"></span>
			                                        </button>
			                                        <button type="button" onclick="window.location.href='adminDodajUzytkownika.html?userId=${user.id}'">
			                                        <span class="glyphicon glyphicon-pencil"></span>
			                                        </button>
			                                        <button type="button" onclick="window.location.href='adminDeleteUzytkownik/${user.id}.html'">
			                                        <span class="glyphicon glyphicon-trash"></span>
			                                        </button>
			                                    </td>
			                                </tr>
			                                </c:forEach>
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</c:if>
		<c:if  test="${empty userList}">
				<div class="text-center" id="legend">
		      		<legend class=""><spring:message code="adminListaUzytkownikow.empty"/></legend>
				</div>
		</c:if>
	</body>
</html>