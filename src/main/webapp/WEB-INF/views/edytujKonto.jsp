<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	</head>
	<body>
	 	<div id="legend">
	      <legend class=""><spring:message code="edytujKonto.legenda"/></legend>
	    </div>
		<c:if test="${success}">
			<div class="alert alert-success">
	 				<strong><spring:message code="edytujKonto.good"/></strong>
			</div>	
		</c:if>
		<form:form class="form-horizontal" action='editUser.html' method="POST" modelAttribute="edit">
		  	<fieldset>
			    <div class="control-group">
			    	<form:hidden path="id"/>
			    </div>
				<div class="control-group">
					<label class="control-label" for="name"><spring:message code="edytujKonto.imie"/></label>
			      	<spring:bind path="name">
	            		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="name" path="name" name="name" placeholder="" class="input-xlarge"/>
			      			<form:errors path="name"></form:errors>
	            		</div>
					</spring:bind>
			    </div>
			    <div class="control-group">
			    	<label class="control-label"  for="surname"><spring:message code="edytujKonto.nazwisko"/></label>
			      	<spring:bind path="surname">
	            		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="surname" path="surname"  name="surname" placeholder="" class="input-xlarge"/>
			      			<form:errors path="surname"></form:errors>
	            		</div>
					</spring:bind>
			    </div>
			    <div class="control-group">
			    	<label class="control-label"  for="email"><spring:message code="edytujKonto.email"/></label>
			      	<spring:bind path="email">
	            		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="email" path="email"  name="email" placeholder="" class="input-xlarge"/>
			      			<p class="help-block"><spring:message code="edytujKonto.email.help"/></p>
			      			<form:errors path="email"></form:errors>
	            		</div>
					</spring:bind>
			    </div>
			    <div class="control-group">
			      	<label class="control-label"  for="pesel"><spring:message code="edytujKonto.pesel"/></label>
			     	<spring:bind path="pesel">
	            		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="pesel" path="pesel"  name="pesel" placeholder="" class="input-xlarge"/>
			      			<p class="help-block"><spring:message code="edytujKonto.pesel.help"/></p>
			      			<form:errors path="pesel"></form:errors>
	            		</div>
					</spring:bind>
			    </div>
			    <div class="control-group">
			    	<label class="control-label"  for="phoneNumber"><spring:message code="edytujKonto.nrtelefonu"/></label>
			     	<spring:bind path="phoneNumber">
	            		<div class="controls ${status.error ? 'has-error' : ''}">
			        		<form:input type="text" id="phoneNumber" path="phoneNumber"  name="phoneNumber" placeholder="" class="input-xlarge"/>
			      			<p class="help-block"><spring:message code="edytujKonto.nrtelefonu.help"/></p>
			      			<form:errors path="phoneNumber"></form:errors>
	            		</div>
					</spring:bind>
			    </div>
			    <div class="control-group">
			    	<label class="control-label"  for="dataUrodzenia"><spring:message code="edytujKonto.dataurodzenia"/></label>
			     	<spring:bind path="dataUrodzenia">
	            		<div class="controls ${status.error ? 'has-error' : ''}">
			         		<form:input type="text" id="dataUrodzenia" path="dataUrodzenia" class="controls" placeholder="YYYY-MM-DD"></form:input>
	           				<p class="help-block"><spring:message code="edytujKonto.dataurodzenia.help"/></p>
	           				<form:errors path="dataUrodzenia"></form:errors>
	            		</div>
					</spring:bind>
			    </div>
			    <br>
			    <label>
			    	<spring:message code="edytujKonto.wymagane.opis"/>
			    </label>
			    <br>
			    <div class="control-group">
			    	<div class="controls">
			        	<button class="btn btn-success"><spring:message code="edytujKonto.przycisk"/></button>
			      	</div>
			    </div>
		  	</fieldset>
		</form:form>
	</body>
</html>