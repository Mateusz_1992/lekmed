<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
 	</head>
	<body>
	 	<div id="legend">
	    	<legend class="">Lista wizyt - dla danego lekarza</legend>
	    </div>
		<c:if  test="${!empty userAppointmentList}">
			<div class="container bootstrap snippet" style="width: 99%;">
			    <div class="row">
			        <div class="col-lg-12">
			            <div class="main-box no-header clearfix">
			                <div class="main-box-body clearfix">
			                    <div class="table-responsive">
			                        <table class="table user-list">
			                            <thead>
			                                <tr class="active">
			                                <th class="text-center"><span><spring:message code="userListaWizyt.id"/></span></th>
			                                <th class="text-center"><span><spring:message code="userListaWizyt.date"/></span></th>
			                                <th class="text-center"><span><spring:message code="userListaWizyt.hour"/></span></th>
			                                <th class="text-center"><span><spring:message code="userListaWizyt.roomNumber"/></span></th>
			                                <th class="text-center"><span><spring:message code="userListaWizyt.specjalizacja"/></span></th> 
			                                <th class="text-center"><span><spring:message code="userListaWizyt.patient"/></span></th>
			                                <th>&nbsp;</th>
			                                </tr>
			                            </thead>
			                            <tbody>
				                            <c:forEach items="${userAppointmentList}" var="appointment" varStatus="status">
				                                <tr>
				                                    <td class="text-center">
				                                        <a class="user-link">${status.index+1}</a>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${appointment.date}</a>
				                                    </td>
				                                    <td class="text-center">
				                                        <a class="user-link">${appointment.hour}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link">${appointment.user.room.roomNumber}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link">${appointment.user.room.specialization}</a>
				                                    </td>
				                                    <td class="text-center">
				                                    	<a class="user-link">${appointment.patient.name} ${appointment.patient.surname}</a>
				                                    </td>
				                                    <td>
				                                    <button type="button" onclick="window.location.href='/lekmed/userRachunki/${appointment.id}.html'">
				                                        <span class="glyphicon glyphicon-barcode"></span>
				                                    </button>
				                                    </td>
				                                </tr>
				                        	</c:forEach>
			                            </tbody>
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</c:if>
		<c:if  test="${empty userAppointmentList}">
			<div class="text-center" id="legend">
	      		<span><spring:message code="userListaWizyt.empty"/></span>
			</div>
		</c:if>
	</body>
</html>