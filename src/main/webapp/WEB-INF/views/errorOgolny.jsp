<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="<c:url value="/resources/bootstrap-3.3.6/dist/js/bootstrap.min.js"/>"></script>
	
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/resources/bootstrap-3.3.6/dist/css/bootstrap.min.css"/>" rel="stylesheet">
	
	    <!-- Custom styles for this template -->
	    <link href="<c:url value="/resources/css/carousel.css" />" rel="stylesheet">
 	</head>
	<body>
		<div class="container">
		  	<div class="row">
		    	<div class="span12">
			      	<div class="hero-unit center">
			          	<div class="text-center">
		                 	<h1 class="without-margin"><spring:message code="errorOgolny.header1"/></h1>
			                <c:if test="${bladListaUzytkownikow}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladListaUzytkownikow"/></h4>
			                </c:if>
			                <c:if test="${bladDodajUserAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladDodajUserAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladEdycjaUserAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEdycjaUserAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladEditHasloForm}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditHasloForm"/></h4>
			                </c:if>
			                <c:if test="${bladEditHaslo}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditHaslo"/></h4>
			                </c:if>
			                <c:if test="${bladAktywacjaUserAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladAktywacjaUserAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladUsunieciaUzytkownika}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladUsunieciaUzytkownika"/></h4>
			                </c:if>
			                <c:if test="${bladEditRolaAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditRolaAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladEditRolaUser}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditRolaUser"/></h4>
			                </c:if>
			                <c:if test="${bladEditRolaClient}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditRolaClient"/></h4>
			                </c:if>
			                <c:if test="${bladListaAdresow}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladListaAdresow"/></h4>
			                </c:if>
			                <c:if test="${bladListaWizytAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladListaWizytAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladRachunekAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladRachunekAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladDodajAdresAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladDodajAdresAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladDodajAdresFormAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladDodajAdresFormAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladDelAdresAdmin}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladDelAdresAdmin"/></h4>
			                </c:if>
			                <c:if test="${bladEditUser}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditUser"/></h4>
			                </c:if>
			                <c:if test="${bladEditAddres}">
			                <h4 class="text-warning"><spring:message code="errorOgolny.bladEditAddres"/></h4>
			                </c:if>
		                 	<h2 class="without-margin"><spring:message code="errorOgolny.header1"/></h2> 
					   	</div>
					   	<div class="text-center">
			      		 	<a href="/lekmed" class="btn btn-large btn-info"><i class="icon-home icon-white"></i><spring:message code="errorOgolny.przycisk"/></a>
			      	   	</div>
			      	</div>
			    </div>
			</div>
		</div>
	</body>
</html>