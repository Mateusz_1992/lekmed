package pl.AAMCH.lekmed.domain;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class QuadaraticEquationTest {

	QuadaraticEquation tempEquation;
	
	@Before
	public void prepareEnvironment()
	{
		tempEquation = new QuadaraticEquation(4,9,3);
	}
	


	@Test
	public void testEquation()
	{
		tempEquation.countDelta();
		tempEquation.countSolutions();
		assertTrue("a cofficient == 0", tempEquation.getCoefficientA() != 0);
		
		assertEquals("Delta should be: 33", 33, tempEquation.getDelta(), 0.01);
		assertEquals("SolutionA should be: -1.84", -1.84, tempEquation.getSolutionI(), 0.01);
		assertEquals("SolutionB should be: -0.4", -0.4, tempEquation.getSolutionII(), 0.01);
	}
	
	@Test
	public void testEquation2()
	{
		tempEquation.countDelta();
		tempEquation.countSolutions();
		assertTrue("a cofficient == 0", tempEquation.getCoefficientA() != 0);
		
		assertEquals("Delta should be: 17", 17, tempEquation.getDelta(), 0.01);
	}


}